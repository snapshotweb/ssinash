<?php
/**
 * Plugin Name: SSI Google Analytics
 * Plugin URI: http://snapshotinteractive.com
 * Description: Adds a Google analytics tracking code to the theme by hooking to wp_head.
 * Author: Chance Strickland
 * Text Domain: ssi-google-analytics
 * Version: 2.0
 */

function ssi_google_analytics() {
  // Tracking code property ID
  $property_id = 'UA-13004152-7';
  ?>

<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $property_id; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() {
    dataLayer.push(arguments)
  };
  gtag( 'js', new Date() );
  gtag( 'config', '<?php echo $property_id; ?>' );
</script>

<?php }
add_action( 'wp_head', 'ssi_google_analytics', 10 );
