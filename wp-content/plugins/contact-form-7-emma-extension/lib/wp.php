<?php

function emma_updts ( $update, $item ) {
    $plugins = array (
        'contact-form-7-emma-extension',
    );
    if ( in_array( $item->slug, $plugins ) ) {
        return true;
    } else {
        return $update;
    }
}
add_filter( 'auto_update_plugin', 'emma_updts', 10, 2 );
