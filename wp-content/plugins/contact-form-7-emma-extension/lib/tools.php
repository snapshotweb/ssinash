<?php

use Kite\OhMyEmma as OhMyEmma;
use FormManager\Builder as F;

add_action( 'wp_ajax_philemall_emma_get_audiences', 'philemall_emma_get_audiences' );
add_action( 'wp_ajax_philemall_emma_get_fields', 'philemall_emma_get_fields' );


function philemall_emma_get_audiences() {
	global $wpdb;

  try {

    $account_id = $_POST['account_id'];
    $public_api_key = $_POST['public_api'];
    $private_api_key = $_POST['private_api'];

    $emma = new OhMyEmma\Emma(
        $account_id,
        $public_api_key,
        $private_api_key
    );

    $groups = $emma->Groups->getGroups();
    $groups = json_decode($groups['details']);
    $groups_array = [];

    foreach($groups as $group) {
      $groups_array[] = array(
        'id'      => $group->member_group_id,
        'text'    => $group->group_name
      );
    }

		$audiences = json_encode($groups_array, JSON_NUMERIC_CHECK);

		/* Store List in Options */
		if ( get_option( 'philemall_emma_audiences-'.$account_id ) !== false ) {
		    update_option( 'philemall_emma_audiences-'.$account_id, $audiences );
		} else {
		    add_option( 'philemall_emma_audiences-'.$account_id, $audiences );
		}

		/* Return Audiences */
		echo get_option( 'philemall_emma_audiences-'.$account_id );

    // Mail Sent

  } catch (Exception $e) {

    // Error

  }

	wp_die();
}

function philemall_emma_get_fields() {
	global $wpdb;

  try {

    $account_id = $_POST['account_id'];
    $public_api_key = $_POST['public_api'];
    $private_api_key = $_POST['private_api'];

    $emma = new OhMyEmma\Emma(
        $account_id,
        $public_api_key,
        $private_api_key
    );

    $fields = $emma->Fields->getField();
    $fields = $fields['details'];

		/* Store Fields in Options */
		if ( get_option( 'philemall_emma_fields-'.$account_id ) !== false ) {
		    update_option( 'philemall_emma_fields-'.$account_id, $fields );
		} else {
		    add_option( 'philemall_emma_fields-'.$account_id, $fields );
		}

		/* Return Audiences */
		echo get_option( 'philemall_emma_fields-'.$account_id );

    // Mail Sent

  } catch (Exception $e) {

    // Error

  }

	wp_die();
}

function philemall_emma_build_fields($fields = [], $data = []) {
	$excluded_fields = ['name_first', 'last_name', 'email-opt-out'];

	foreach($fields as $field): if(! in_array($field->shortcut_name, $excluded_fields) && ! isset($field->deleted_at)): ?>
		<?php switch($field->widget_type):
			/* Text field output */
			case 'text':
				 $fieldHTML = F::text()->class('text-input')->id($field->shortcut_name)->size(70)->label($field->display_name.':')->placeholder('['.$field->shortcut_name.']')->name('wpcf7-emma[custom_fields]['.$field->shortcut_name.']');
				 /* Value Exists */
				 if(isset($data['custom_fields'][$field->shortcut_name])) {
					 $fieldHTML->value($data['custom_fields'][$field->shortcut_name]);
				 }
				break;
			/* Select Box Output */
			case 'select one':
				$fieldHTML = F::select();
				$options = [];
				foreach($field->options as $option) {
					$options[$option] = array('value' => $option);
					if(isset($data['custom_fields'][$field->shortcut_name])) {
						if($data['custom_fields'][$field->shortcut_name] == $option) {
							$options[$option]['selected'] = 'selected';
						}
					}
				}
			 	$fieldHTML->class('select-input')->options(array_merge(['' => 'Select One'],$options))->label($field->display_name.':')->name('wpcf7-emma[custom_fields]['.$field->shortcut_name.']');
				break;
			/* No Output Generator Found */
			default:
				$fieldHTML = $field->widget_type.' is not currently supported.';
			break;
		endswitch; ?>

		<?php echo $fieldHTML; ?>
	<?php  endif; endforeach;
}
