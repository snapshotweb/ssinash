<?php

function wpcf7_emma_admin_enqueue_scripts() {

	global $plugin_page;

	if ( ! isset( $plugin_page ) || 'wpcf7' != $plugin_page )
		return;

	wp_enqueue_style( 'select2-admin', PHILEMALL_EMMA_PLUGIN_URL . '/assets/css/select2.min.css', array(), PHILEMALL_EMMA_VERSION, 'all' );
	wp_enqueue_style( 'wpcf7-emma-admin', PHILEMALL_EMMA_PLUGIN_URL . '/assets/css/style-emma.css', array(), PHILEMALL_EMMA_VERSION, 'all' );

	wp_enqueue_script( 'select2-admin', PHILEMALL_EMMA_PLUGIN_URL . '/assets/js/select2.full.min.js', array( 'jquery' ), PHILEMALL_EMMA_VERSION, true );
	wp_enqueue_script( 'wpcf7-emma-admin', PHILEMALL_EMMA_PLUGIN_URL . '/assets/js/scripts-emma.js', array( 'jquery', 'wpcf7-admin' ), PHILEMALL_EMMA_VERSION, true );


}
add_action( 'admin_print_scripts', 'wpcf7_emma_admin_enqueue_scripts' );


/* Custom ajax loader */
function wpcf7_emma_ajax_loader() {

	return  PHILEMALL_EMMA_PLUGIN_URL . '/assets/images/fading-squares.gif';

}
add_filter('wpcf7_ajax_loader', 'wpcf7_emma_ajax_loader');
