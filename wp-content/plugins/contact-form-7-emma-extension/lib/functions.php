<?php

use Kite\OhMyEmma as OhMyEmma;

add_filter( 'wpcf7_editor_panels', 'show_emma_metabox' );
add_action( 'wpcf7_after_save', 'wpcf7_emma_save_emma' );
add_action( 'wpcf7_before_send_mail', 'wpcf7_emma_subscribe' );
add_filter( 'wpcf7_form_class_attr', 'philemall_emma_class_attr' );


function wpcf7_emma_add_emma($args) {
  $cf7_emma_defaults = array();
  $cf7_emma = get_option( 'cf7_emma_'.$args->id(), $cf7_emma_defaults );

  $host = esc_url_raw( $_SERVER['HTTP_HOST'] );
  $url = $_SERVER['REQUEST_URI'];
  $urlactual = $url;
?>

<div class="metabox-holder">

  <h3>MyEmma Extension v.<?php echo PHILEMALL_EMMA_VERSION ?></h3>

  <div class="emma-main-fields">

    <p class="mail-field">
      <label for="wpcf7-emma-name"><?php echo esc_html( __( 'Subscriber Name:', 'wpcf7' ) ); ?></label> <span data-tooltip="Field will be split into first and last name Emma fields with a space delimeter."><i class="emma-tool-tip">?</i></span> <br />
      <input type="text" id="wpcf7-emma-name" name="wpcf7-emma[name]" class="wide" size="70" placeholder="[your-name] <= Make sure this the name of your form field" value="<?php echo (isset ($cf7_emma['name'] ) ) ? esc_attr( $cf7_emma['name'] ) : ''; ?>" />
    </p>


    <p class="mail-field">
      <label for="wpcf7-emma-email"><?php echo esc_html( __( 'Subscriber Email:', 'wpcf7' ) ); ?></label><br />
      <input type="text" id="wpcf7-emma-email" name="wpcf7-emma[email]" class="wide" size="70" placeholder="[your-email] <= Make sure this the name of your form field" value="<?php echo (isset ( $cf7_emma['email'] ) ) ? esc_attr( $cf7_emma['email'] ) : ''; ?>" />
    </p>


    <p class="mail-field">
      <label for="wpcf7-emma-public-api"><?php echo esc_html( __( 'Emma Public API Key:', 'wpcf7' ) ); ?></label>  <br />
      <input type="text" id="wpcf7-emma-public-api" name="wpcf7-emma[public_api]" class="wide" size="70" placeholder="d95e09fa6cdd0699014d" value="<?php echo (isset($cf7_emma['public_api']) ) ? esc_attr( $cf7_emma['public_api'] ) : ''; ?>" />
    </p>

    <p class="mail-field">
      <label for="wpcf7-emma-private-api"><?php echo esc_html( __( 'Emma Private API Key:', 'wpcf7' ) ); ?> </label> <br />
      <input type="text" id="wpcf7-emma-private-api" name="wpcf7-emma[private_api]" class="wide" size="70" placeholder="cb1216d55a2639f96c604" value="<?php echo (isset($cf7_emma['private_api']) ) ? esc_attr( $cf7_emma['private_api'] ) : ''; ?>" />
    </p>

    <p class="mail-field">
      <label for="wpcf7-emma-account-id"><?php echo esc_html( __( 'Emma Account ID:', 'wpcf7' ) ); ?></label>  <br />
      <input type="text" id="wpcf7-emma-account-id" name="wpcf7-emma[account_id]" class="wide" size="70" placeholder="1342546" value="<?php echo (isset($cf7_emma['account_id']) ) ? esc_attr( $cf7_emma['account_id'] ) : ''; ?>" />
    </p>

    <p class="mail-field" id="wpcf7-emma-audience-field-container" <?php echo ( isset($cf7_emma['audience_select_active'])?'style="display: none"':''); ?>>
      <label for="wpcf7-emma-audience-field"><?php echo esc_html( __( 'Emma Audience ID:', 'wpcf7' ) ); ?></label> <span data-tooltip="Comma separated list of Audience ID's or Hidden Audience field."><i class="emma-tool-tip">?</i></span>   <br />
      <input type="text" id="wpcf7-emma-audience-field" name="wpcf7-emma[audience_field]" class="wide" size="70" placeholder="18689434" value="<?php echo (isset($cf7_emma['audience_field']) ) ? esc_attr( $cf7_emma['audience_field'] ) : ''; ?>" />
    </p>

    <p class="mail-field">
      <input type="checkbox" id="wpcf7-emma-audience-select-active" name="wpcf7-emma[audience_select_active]" value="1" <?php echo ( isset($cf7_emma['audience_select_active']) ) ? ' checked="checked"' : ''; ?> />
      <label for="wpcf7-emma-audience-select-active"><?php echo esc_html( __( 'Use Audience Selector', 'wpcf7' ) ); ?></label> <span data-tooltip="Pull in a list of your Emma Audiences."><i class="emma-tool-tip">?</i></span>
    </p>

    <div id="wpcf7-emma-audience-select-container" <?php echo (! isset($cf7_emma['audience_select_active'])?'style="display: none"':''); ?>>
      <p id="emma-audiences" class="mail-field" <?php echo (! isset($cf7_emma['audiences'])?'style="display: none"':''); ?>>
        <select class="emma-audiences" multiple="multiple" name="wpcf7-emma[audiences][]">
            <?php if(isset($cf7_emma['audiences'])) {
              $audiences = json_decode(get_option( 'philemall_emma_audiences-'.$cf7_emma['account_id'] ));

              foreach($audiences as $audience) {
                echo '<option value="'.$audience->id.'" '.(in_array($audience->id, $cf7_emma['audiences'])?'selected="selected"':'').'>'.$audience->text.'</option>';
              }
            } ?>
        </select>

        <?php if(isset($audiences) && isset($cf7_emma['audiences'])): ?>
          <script>
          var selected_audiences = <?php echo json_encode( $cf7_emma['audiences'] ); ?>;

          jQuery(document).ready(function() {
            jQuery("#emma-audiences select").select2({
              multiple: true,
            });
          });
          </script>
        <?php endif; ?>
      </p>

      <p class="mail-field">
        <button id="load-audiences" class="button-primary"><?php echo (isset($cf7_emma['audiences'])?'Reload':'Load'); ?> Audiences</button>
      </p>
    </div>

  </div>

  <div class="cme-container emma-support">

    <p class="mail-field">
    <input type="checkbox" id="wpcf7-emma-cf-active" name="wpcf7-emma[cfactive]" value="1" <?php echo ( isset($cf7_emma['cfactive']) ) ? ' checked="checked"' : ''; ?> />
    <label for="wpcf7-emma-cf-active"><?php echo esc_html( __( 'Use Custom Fields', 'wpcf7' ) ); ?></label> <span data-tooltip="Map form data to Emma custom fields."><i class="emma-tool-tip">?</i></span>
    </p>

  </div>


  <div class="emma-custom-fields"  <?php echo (! isset($cf7_emma['cfactive'])?'style="display: none"':''); ?>>
    <!-- CUSTOM FIELD CODE -->
    <?php
      $fields = json_decode(get_option( 'philemall_emma_fields-'.$cf7_emma['account_id'] ));
      philemall_emma_build_fields($fields, $cf7_emma);
    ?>

    <p class="mail-field">
      <button id="load-fields" class="button-primary"><?php echo (isset($cf7_emma['custom_fields'])?'Reload':'Load'); ?> Fields</button> <span data-tooltip="This action saves the contact form."><i class="emma-tool-tip">?</i></span>
    </p>

  </div>

</div>


<?php

}

function wpcf7_emma_save_emma($args) {

  if (!empty($_POST)){
    update_option( 'cf7_emma_'.$args->id(), $_POST['wpcf7-emma'] );
  }

}


function show_emma_metabox( $panels ) {

  $new_page = array(
    'Emma-Extension' => array(
      'title' => __( 'MyEmma', 'contact-form-7' ),
      'callback' => 'wpcf7_emma_add_emma'
    )
  );

  $panels = array_merge($panels, $new_page);

  return $panels;

}


function cf7_emma_tag_replace( $pattern, $subject, $posted_data, $html = false ) {

  if( preg_match($pattern,$subject,$matches) > 0 )
  {

    if ( isset( $posted_data[$matches[1]] ) ) {
      $submitted = $posted_data[$matches[1]];

      if ( is_array( $submitted ) )
        $replaced = join( ', ', $submitted );
      else
        $replaced = $submitted;

      if ( $html ) {
        $replaced = strip_tags( $replaced );
        $replaced = wptexturize( $replaced );
      }

      $replaced = apply_filters( 'wpcf7_mail_tag_replaced', $replaced, $submitted );

      return stripslashes( $replaced );
    }

    if ( $special = apply_filters( 'wpcf7_special_mail_tags', '', $matches[1] ) )
      return $special;

    return $matches[0];
  }
  return $subject;

}

function cf7_emma_tag_replace_all( $pattern, $subject, $posted_data, $html = false ) {

  if( preg_match_all($pattern,$subject,$matches) > 0)
  {
    if(isset($matches[1])) {
      $submitted = $subject;

      foreach( $matches[1] as $match ) {

          $submitted = preg_replace('/\['.$match.'\]/i', $posted_data[$match], $submitted);

          if ( is_array( $submitted ) )
            $replaced = join( ', ', $submitted );
          else
            $replaced = $submitted;

          if ( $html ) {
            $replaced = strip_tags( $replaced );
            $replaced = wptexturize( $replaced );
          }

          $replaced = apply_filters( 'wpcf7_mail_tag_replaced', $replaced, $submitted );

      }

      return stripslashes( $replaced );
    }

    if ( $special = apply_filters( 'wpcf7_special_mail_tags', '', $matches[1] ) )
      return $special;

    return $matches[0];
  }
  return $subject;

}


function wpcf7_emma_subscribe($obj) {
  $cf7_emma = get_option( 'cf7_emma_'.$obj->id() );

  $submission = WPCF7_Submission::get_instance();

  if ( $submission ) {
    $posted_data = $submission->get_posted_data();
  }

  if( $cf7_emma ) {
    $subscribe = false;

    $regex = '/\[\s*([a-zA-Z_][0-9a-zA-Z:._-]*)\s*\]/';
    $callback = array( &$obj, 'cf7_emma_callback' );

    $email = cf7_emma_tag_replace_all( $regex, $cf7_emma['email'], $submission->get_posted_data() );
    $name = cf7_emma_tag_replace_all( $regex, $cf7_emma['name'], $submission->get_posted_data() );

    if( ! isset($cf7_emma['audience_select_active'])) {
      $audience_field = cf7_emma_tag_replace_all( $regex, $cf7_emma['audience_field'], $submission->get_posted_data() );
      $audiences = explode(',', $audience_field);
    } else {
      $audiences = $cf7_emma['audiences'];
    }

    $merge_vars= array( 'name_first' => $name);

        $parts = explode(" ", $name);
        if(count($parts)>1) {

          $lastname = array_pop($parts);
          $firstname = implode(" ", $parts);
          $merge_vars= array('name_first' => $firstname, 'last_name' => $lastname);

        } else {
          $merge_vars = array('name_first' => $name);
        }

    /* Custom Field Logic */

    if( isset($cf7_emma['custom_fields']) && isset($cf7_emma['cfactive']) )
    {
      foreach($cf7_emma['custom_fields'] as $custom_field => $value) {
        if($value != '') {
          $merge_vars = array_merge($merge_vars, array($custom_field => cf7_emma_tag_replace_all( $regex, trim($value), $submission->get_posted_data() ) ) );
        }
      }
    }

    /* Do Subscription */
    if($email != $cf7_emma['email'])
    {

      $account_id = $cf7_emma['account_id'];
      $public_api_key = $cf7_emma['public_api'];
      $private_api_key = $cf7_emma['private_api'];

      try {

        $emma = new OhMyEmma\Emma(
            $account_id,
            $public_api_key,
            $private_api_key
        );

        $subscriber = array(
            'email'       => $email,
            'fields'      => $merge_vars,
            'group_ids'   => $audiences
        );

        $emma->Members->updateAddMember($subscriber);


        // Mail Sent

      } catch (Exception $e) {

        // Error

      }
    }

  }

}

function philemall_emma_class_attr( $class ) {

  $class .= ' emma-ext-' . PHILEMALL_EMMA_VERSION;
  return $class;

}
