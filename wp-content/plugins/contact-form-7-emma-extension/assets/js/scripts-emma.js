jQuery(document).ready(function() {

	try {
		/* Load Audiences from Emma on click */
		jQuery('#load-audiences').click(function(e) {
			e.preventDefault();

			if(
				jQuery('#wpcf7-emma-account-id').val() != '' &&
				jQuery('#wpcf7-emma-public-api').val() != '' &&
				jQuery('#wpcf7-emma-private-api').val() != ''
			) {
				var data = {
					'action': 'philemall_emma_get_audiences',
					'account_id': jQuery('#wpcf7-emma-account-id').val(),
					'public_api': jQuery('#wpcf7-emma-public-api').val(),
					'private_api': jQuery('#wpcf7-emma-private-api').val(),
				};

				jQuery.post(ajaxurl, data, function(data) {
					jQuery('#emma-audiences select option').remove();
					jQuery("#emma-audiences select").select2({
						multiple: true,
						data: JSON.parse(data)
					});

					jQuery('#emma-audiences').show();

					if(selected_audiences) {
						jQuery("#emma-audiences select").val(selected_audiences).trigger("change");
					}
				});
			} else {
				alert('Please fill out api keys and account id.')
			}
		});


		/* Load Fields from Emma on click */
		jQuery('#load-fields').click(function(e) {
			e.preventDefault();

			if(
				jQuery('#wpcf7-emma-account-id').val() != '' &&
				jQuery('#wpcf7-emma-public-api').val() != '' &&
				jQuery('#wpcf7-emma-private-api').val() != ''
			) {
				var data = {
					'action': 'philemall_emma_get_fields',
					'account_id': jQuery('#wpcf7-emma-account-id').val(),
					'public_api': jQuery('#wpcf7-emma-public-api').val(),
					'private_api': jQuery('#wpcf7-emma-private-api').val(),
				};

				jQuery.post(ajaxurl, data, function(data) {
					/* Submit Form */
					jQuery('#wpcf7-admin-form-element').submit();
				});

			} else {
				alert('Please fill out api keys and account id.')
			}
		});

		/* Show / Hide Custom Fields */

		jQuery('#wpcf7-emma-cf-active').change(function(e){
			if(jQuery(this).is(':checked')) {
				jQuery('.emma-custom-fields').show();
			} else {
				jQuery('.emma-custom-fields').hide();
			}
		});

		jQuery('#wpcf7-emma-audience-select-active').change(function(e){
			if(jQuery(this).is(':checked')) {
				jQuery('#wpcf7-emma-audience-select-container').show();
				jQuery('#wpcf7-emma-audience-field-container').hide();
			} else {
				jQuery('#wpcf7-emma-audience-select-container').hide();
				jQuery('#wpcf7-emma-audience-field-container').show();
			}
		});

	}

	catch (e) {

	}

});
