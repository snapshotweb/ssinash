<?php
/*
Plugin Name: Contact Form 7 Emma Extension
Plugin URI: http://philipbennett.net
Description: Integrate Contact Form 7 with MyEmma. Automatically add form submissions to predetermined lists in MyEmma, using its latest API.
Author: Phil Bennett
Author URI: http://philipbennett.net
Text Domain: contact-form-7
Domain Path: /languages/
Version: 0.1
*/

/*  Copyright 2016 Phil Bennett (email: webmstrphil at gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


define( 'PHILEMALL_EMMA_VERSION', '0.1' );
define( 'PHILEMALL_EMMA_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'PHILEMALL_EMMA_PLUGIN_NAME', trim( dirname( PHILEMALL_EMMA_PLUGIN_BASENAME ), '/' ) );
define( 'PHILEMALL_EMMA_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
define( 'PHILEMALL_EMMA_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );
define( 'PHILEMALL_URL', 'http://bitbucket.com/philemall' );

require_once(PHILEMALL_EMMA_PLUGIN_DIR . '/vendor/autoload.php');
require_once( PHILEMALL_EMMA_PLUGIN_DIR . '/lib/emma.php' );
