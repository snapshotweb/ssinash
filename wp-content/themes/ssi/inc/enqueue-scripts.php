<?php
function ssi_scripts() {

  /**
   * Modernizr
   */
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/dist/js/vendor/modernizr.custom.js', array(), '2.7.1', false );

  /**
   * Classie
   *
   * This is used to support the navigation menu
   */
  wp_enqueue_script( 'classie', get_template_directory_uri() . '/dist/js/vendor/classie.js', array( 'modernizr' ), '1.0.0', true );

  /**
   * Google Fonts
   */
  wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i|Open+Sans:300,300i,400,400i,700,700i,800,800i', array(), '1.0.0' );

  /**
   * Foundation
   */
  wp_enqueue_script( 'foundation', get_template_directory_uri() . '/dist/js/vendor/foundation.min.js', array( 'jquery' ), '6.3.1', true );
  wp_enqueue_style( 'foundation-style', get_template_directory_uri() . '/dist/css/foundation.min.css', array(), '6.3.1' );

  /**
   * Masonry
   *
   * Only needed on the homepage for the `Behind the Scenese` block_local_requests
   */
  if ( is_front_page() ) {
    wp_enqueue_script( 'masonry', '//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', array( 'jquery' ), '4.1.1', true );
  }

  /**
   * Isotope
   */
  wp_enqueue_script( 'isotope', '//unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array( 'jquery' ) );

  /**
   * Snazzy Maps
   */
  if ( is_page_template( 'page-templates/page-contact.php' ) ) {
    $maps_api_key      = 'AIzaSyD2GmBipe-50Jn1hstMU_p-ILkLxOyx_Ng';
    $snazzy_js         = '/dist/js/map.js';
    $translation_array = array( 'templateURL' => get_stylesheet_directory_uri() );
    wp_enqueue_script( 'google-maps', '//maps.googleapis.com/maps/api/js?key=' . $maps_api_key, array(), null, false );

    if ( wp_script_is( 'google-maps', 'enqueued' ) ) {
      wp_register_script( 'snazzy-maps', get_template_directory_uri() . $snazzy_js, array( 'google-maps' ), filemtime( get_template_directory() . $snazzy_js ), true );
      wp_localize_script( 'snazzy-maps', 'directoryURI', $translation_array );
      wp_enqueue_script( 'snazzy-maps' );
    }
  }

  /**
   * Slick Slider
   */
  wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array( 'jquery' ), '1.6.0', true );
  if ( wp_script_is( 'slick', 'enqueued' ) ) {
    wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', array(), '1.6.0' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/dist/css/slick-theme.css', array( 'slick-style' ), '0.0.1' );
  }

  /**
   * Lity
   */
  wp_enqueue_script( 'lity', get_template_directory_uri() . '/dist/js/vendor/lity.min.js', array( 'jquery' ), '2.2.2', true );
  if ( wp_script_is( 'lity', 'enqueued' ) ) {
    wp_enqueue_style( 'lity-style', get_template_directory_uri() . '/dist/css/lity.min.css', array(), '2.2.2' );
  }

  /**
   * FancyBox
   */
  $fancy_css = '/dist/css/jquery.fancybox.min.css';
  $fancy_js  = '/dist/js/vendor/jquery.fancybox.min.js';
  wp_enqueue_script( 'fancybox', get_template_directory_uri() . $fancy_js, array( 'jquery' ), '3.1.25', true );
  wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . $fancy_css, array(), '3.1.25' );


  /**
   * Theme styles and scripts
   */
  $theme_css = '/dist/css/app.min.css';
  $theme_js  = '/dist/js/app.min.js';
  $ajaxurl = admin_url( 'admin-ajax.php');
  wp_enqueue_style( 'ssi-style', get_template_directory_uri() . $theme_css, array( 'foundation-style' ), filemtime( get_template_directory() . $theme_css ) );
  wp_enqueue_script( 'ssi-navigation', get_template_directory_uri() . '/dist/js/navigation.min.js', array(), '0.0.1', true );
  wp_enqueue_script( 'ssi-skip-link-focus-fix', get_template_directory_uri() . '/dist/js/skip-link-focus-fix.min.js', array(), '20170407', true );
  wp_register_script( 'ssi-script', get_template_directory_uri() . $theme_js, array( 'jquery' ), filemtime( get_template_directory() . $theme_js ), true );
  // Localize for Ajax functions
  wp_localize_script( 'ssi-script', 'ajaxLoadPosts', array(
    'ajaxurl'       => $ajaxurl,
    'loading'       => esc_html__( 'Loading …', 'ssi' ),
    'noposts'       => esc_html__( 'No older posts found', 'ssi' ),
    'loadmore'      => esc_html__( 'Load more', 'ssi' ),
  ) );
  // Localize for running conditional JS functions around third-party libraries
  wp_localize_script( 'ssi-script', 'scriptIsEnqueued', array(
    'lity'          => wp_script_is( 'lity', 'enqueued' ),
    'isotope'       => wp_script_is( 'isotope', 'enqueued' ),
    'masonry'       => wp_script_is( 'masonry', 'enqueued' ),
    'google-maps'   => wp_script_is( 'google-maps', 'enqueued' ),
    'slick'         => wp_script_is( 'slick', 'enqueued' ),
  ) );
  wp_enqueue_script( 'ssi-script' );

  /**
   * Comment scripts
   */
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'ssi_scripts' );
