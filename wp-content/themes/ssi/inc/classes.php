<?php
/**
 * Add ACF Subfields to Relevanssi search index
 *
 * @package     Relevanssi ACF subfields
 * @link        https://github.com/cftp/relevanssi-acf-subfields
 */
require get_template_directory() . '/inc/classes/Relevanssi-ACF-Subfields.php';

/**
 * Create methods to easily detect mobile devices.
 *
 * @package     Mobile Detect Library
 * @link        http://mobiledetect.net
 * @version     2.8.26
 */
require get_template_directory() . '/inc/classes/Mobile-Detect.php';

/**
 * Customize the output of menus for Foundation top bar
 *
 * @package     SnapShot_Interactive
 */
// require get_template_directory() . '/inc/classes/SSI-Top-Bar-Walker.php';
