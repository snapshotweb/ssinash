<?php
$directory = get_template_directory() . '/inc/post-types/';
$dir = new DirectoryIterator( $directory );
foreach ( $dir as $fileinfo ) {
  if ( !$fileinfo->isDot() ) {
    require $directory . $fileinfo->getFilename();
  }
}

/**
 * Require taxonomies
 */
require get_parent_theme_file_path('/inc/taxonomies.php' );