<?php
$directory = get_template_directory() . '/inc/taxonomies/';
$dir = new DirectoryIterator( $directory );
foreach ( $dir as $fileinfo ) {
  if ( !$fileinfo->isDot() ) {
    require $directory . $fileinfo->getFilename();
  }
}

/**
 * Completely disable term archives for a given taxonomy.
 *
 * @param string $taxonomy WordPress taxnomy name
 */
function ssi_kill_taxonomy_archive( $taxonomy ) {

  add_action( 'pre_get_posts', function( $qry ) {

    if ( is_admin() ) {
      return;
    }
    if ( is_tax( $taxonomy ) ) {
      $qry->set_404();
    }
  });
}
