<?php
add_action( 'init', 'ssi_tax_industry', 0 );

function ssi_tax_industry() {

  $labels = array(
    'name'                => _x( 'Industries', 'taxonomy general name', 'ssi' ),
    'singular_name'       => _x( 'Industry', 'taxonomy singular name', 'ssi' ),
    'search_items'        => __( 'Search Industries', 'ssi' ),
    'all_items'           => __( 'All Industries', 'ssi' ),
    'parent_item'         => __( 'Parent Industry', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Industry:', 'ssi' ),
    'edit_item'           => __( 'Edit Industry', 'ssi' ),
    'update_item'         => __( 'Update Industry', 'ssi' ),
    'add_new_item'        => __( 'Add New Industry', 'ssi' ),
    'new_item_name'       => __( 'New Industry Name', 'ssi' ),
    'menu_name'           => __( 'Industries', 'ssi' ),
  );

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'industry' ),
  );

  register_taxonomy( 'industry', array( 'project', 'client', 'casestudy' ), $args );
}