<?php
$tax_name = 'cta-style';
add_action( 'init', 'ssi_tax_ctastyle', 0 );

function ssi_tax_ctastyle() {

  $labels = array(
    'name'                => _x( 'CTA Styles', 'taxonomy general name', 'ssi' ),
    'singular_name'       => _x( 'CTA Style', 'taxonomy singular name', 'ssi' ),
    'search_items'        => __( 'Search Styles', 'ssi' ),
    'all_items'           => __( 'All Styles', 'ssi' ),
    'parent_item'         => __( 'Parent Style', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Style:', 'ssi' ),
    'edit_item'           => __( 'Edit Styles', 'ssi' ),
    'update_item'         => __( 'Update Style', 'ssi' ),
    'add_new_item'        => __( 'Add New Style', 'ssi' ),
    'new_item_name'       => __( 'New Style', 'ssi' ),
    'menu_name'           => __( 'Styles', 'ssi' ),
  );

  $args = array(
    'hierarchical'        => false,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'cta-style' ),
    'meta_box_cb'         => 'cta_style_meta_box',
  );

  register_taxonomy( 'cta-style', array('call-to-action'), $args );
}

/**
 * Kill the archive.
 */
ssi_kill_taxonomy_archive( 'cta-style' );

/**
 * Display the meta box as radio buttons
 */
function cta_style_meta_box( $post ) {
  $terms  = get_terms( 'cta-style', array( 'hide_empty' => false ) );
  $post   = get_post();
  $style  = wp_get_object_terms( $post->ID, 'cta-style', array( 'orderby' => 'term_id', 'order' => 'ASC' ) );
  $name   = '';
  if ( ! is_wp_error( $style ) ) {
    if ( isset( $style[0] ) && isset( $style[0]->name ) ) {
      $name = $style[0]->name;
    }
  }
  foreach ( $terms as $term ) : ?>

    <label title='<?php esc_attr_e( $term->name ); ?>'>
      <input type="radio" name="cta-style" value="<?php esc_attr_e( $term->name ); ?>" <?php checked( $term->name, $name ); ?> />
      <span><?php esc_html_e( $term->name ); ?></span>
    </label><br />

  <?php
  endforeach;
}

/**
 * Save the movie meta box results.
 *
 * @param int $post_id The ID of the post that's being saved.
 */
function save_cta_style_meta_box( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }
  if ( ! isset( $_POST['cta-style'] ) ) {
    return;
  }
  $style = sanitize_text_field( $_POST['cta-style'] );

  // A valid style is required, so don't let this get published without one
  if ( empty( $style ) ) {
    // unhook this function so it doesn't loop infinitely
    remove_action( 'save_post', 'save_cta_style_meta_box' );
    $postdata = array(
      'ID'          => $post_id,
      'post_status' => 'draft',
    );
    wp_update_post( $postdata );
  }
  else {
    $term = get_term_by( 'name', $style, 'cta-style' );
    if ( ! empty( $term ) && ! is_wp_error( $term ) ) {
      wp_set_object_terms( $post_id, $term->term_id, 'cta-style', false );
    }
  }
}
add_action( 'save_post', 'save_cta_style_meta_box' );
