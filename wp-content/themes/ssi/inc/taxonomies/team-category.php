<?php
add_action( 'init', 'ssi_tax_teamcategory', 0 );

function ssi_tax_teamcategory() {

  $labels = array(
    'name'                => _x( 'Team Categories', 'taxonomy general name', 'ssi' ),
    'singular_name'       => _x( 'Team Category', 'taxonomy singular name', 'ssi' ),
    'search_items'        => __( 'Search Team Categories', 'ssi' ),
    'all_items'           => __( 'All Team Categories', 'ssi' ),
    'parent_item'         => __( 'Parent Category', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Category:', 'ssi' ),
    'edit_item'           => __( 'Edit Team Category', 'ssi' ),
    'update_item'         => __( 'Update Team Category', 'ssi' ),
    'add_new_item'        => __( 'Add New Team Category', 'ssi' ),
    'new_item_name'       => __( 'New Team Category Name', 'ssi' ),
    'menu_name'           => __( 'Team Categories', 'ssi' ),
  );

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'team-category' ),
  );

  register_taxonomy( 'team-category', array('team'), $args );
}