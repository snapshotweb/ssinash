<?php
add_action( 'init', 'ssi_project_type', 0 );

function ssi_project_type() {

  $labels = array(
    'name'                => _x( 'Project Types', 'taxonomy general name', 'ssi' ),
    'singular_name'       => _x( 'Project Type', 'taxonomy singular name', 'ssi' ),
    'search_items'        => __( 'Search Project Types', 'ssi' ),
    'all_items'           => __( 'All Project Types', 'ssi' ),
    'parent_item'         => __( 'Parent Project Type', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Project Type:', 'ssi' ),
    'edit_item'           => __( 'Edit Project Type', 'ssi' ),
    'update_item'         => __( 'Update Project Type', 'ssi' ),
    'add_new_item'        => __( 'Add New Project Type', 'ssi' ),
    'new_item_name'       => __( 'New Project Type Name', 'ssi' ),
    'menu_name'           => __( 'Project Types', 'ssi' ),
  );

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'project-type' ),
  );

  register_taxonomy( 'project-type', array( 'project' ), $args );
}