<?php
add_action( 'init', 'ssi_post_type_calltoaction' );

function ssi_post_type_calltoaction() {

  $labels = array(
    'name'                => _x( 'Calls-to-Action', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Call-to-Action', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Calls-to-Action', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Call-to-Action', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'Add New Call-to-Action', 'ssi' ),
    'add_new_item'        => __( 'Add New Call-to-Action', 'ssi' ),
    'new_item'            => __( 'New Call-to-Action', 'ssi' ),
    'edit_item'           => __( 'Edit Call-to-Action', 'ssi' ),
    'view_item'           => __( 'View Call-to-Action', 'ssi' ),
    'all_items'           => __( 'All Calls-to-Action', 'ssi' ),
    'search_items'        => __( 'Search Calls-to-Action', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Calls-to-Action:', 'ssi' ),
    'not_found'           => __( 'No Calls-to-Action found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No Calls-to-Action found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'call-to-action' ),
    'has_archive'         => false,
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-megaphone'
  );
  register_post_type( 'call-to-action', $args );
}
