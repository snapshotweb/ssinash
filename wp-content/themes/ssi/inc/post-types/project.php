<?php
add_action( 'init', 'ssi_post_type_project' );

function ssi_post_type_project() {

  $labels = array(
    'name'                => _x( 'Projects', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Project', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Projects', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Project', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'project', 'ssi' ),
    'add_new_item'        => __( 'Add New Project', 'ssi' ),
    'new_item'            => __( 'New Project', 'ssi' ),
    'edit_item'           => __( 'Edit Project', 'ssi' ),
    'view_item'           => __( 'View Project', 'ssi' ),
    'all_items'           => __( 'All Projects', 'ssi' ),
    'search_items'        => __( 'Search Projects', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Projects:', 'ssi' ),
    'not_found'           => __( 'No projects found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No projects found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'project' ),
    'has_archive'         => 'projects',
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-images-alt2'
  );
  register_post_type( 'project', $args );
}
