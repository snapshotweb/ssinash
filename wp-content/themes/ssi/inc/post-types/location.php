<?php
add_action( 'init', 'ssi_post_type_location' );

function ssi_post_type_location() {

  $labels = array(
    'name'                => _x( 'Locations', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Location', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Office Locations', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Location', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'location', 'ssi' ),
    'add_new_item'        => __( 'Add New Location', 'ssi' ),
    'new_item'            => __( 'New Location', 'ssi' ),
    'edit_item'           => __( 'Edit Location', 'ssi' ),
    'view_item'           => __( 'View Location', 'ssi' ),
    'all_items'           => __( 'All Locations', 'ssi' ),
    'search_items'        => __( 'Search Locations', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Locations:', 'ssi' ),
    'not_found'           => __( 'No locations found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No locations found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'location' ),
    'has_archive'         => false,
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-location'
  );
  register_post_type( 'location', $args );
}
