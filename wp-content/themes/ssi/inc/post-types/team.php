<?php
add_action( 'init', 'ssi_post_type_team' );

function ssi_post_type_team() {

  $labels = array(
    'name'               => _x( 'Team', 'post type general name', 'ssi' ),
    'singular_name'      => _x( 'Team Member', 'post type singular name', 'ssi' ),
    'menu_name'          => _x( 'Team', 'admin menu', 'ssi' ),
    'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'ssi' ),
    'add_new'            => _x( 'Add New', 'project', 'ssi' ),
    'add_new_item'       => __( 'Add New Team Member', 'ssi' ),
    'new_item'           => __( 'New Team Member', 'ssi' ),
    'edit_item'          => __( 'Edit Team Member', 'ssi' ),
    'view_item'          => __( 'View Team Member', 'ssi' ),
    'all_items'          => __( 'All Team Members', 'ssi' ),
    'search_items'       => __( 'Search Team Members', 'ssi' ),
    'parent_item_colon'  => __( 'Parent Team Member:', 'ssi' ),
    'not_found'          => __( 'No team members found.', 'ssi' ),
    'not_found_in_trash' => __( 'No team members found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'ssi' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'team' ),
    'has_archive'        => __( 'team-member', 'ssi' ),
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'          => 'dashicons-groups'
  );
  register_post_type( 'team', $args );
}
