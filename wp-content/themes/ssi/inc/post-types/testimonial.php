<?php
add_action( 'init', 'ssi_post_type_testimonial' );

function ssi_post_type_testimonial() {

  $labels = array(
    'name'                => _x( 'Testimonials', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Testimonial', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Testimonials', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Testimonial', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'testimonial', 'ssi' ),
    'add_new_item'        => __( 'Add New Testimonial', 'ssi' ),
    'new_item'            => __( 'New Testimonial', 'ssi' ),
    'edit_item'           => __( 'Edit Testimonial', 'ssi' ),
    'view_item'           => __( 'View Testimonial', 'ssi' ),
    'all_items'           => __( 'All Testimonials', 'ssi' ),
    'search_items'        => __( 'Search Testimonials', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Testimonials:', 'ssi' ),
    'not_found'           => __( 'No testimonials found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No testimonials found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'testimonial' ),
    'has_archive'         => false,
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-format-chat'
  );
  register_post_type( 'testimonial', $args );
}
