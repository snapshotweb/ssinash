<?php
add_action( 'init', 'ssi_post_type_casestudy' );

function ssi_post_type_casestudy() {

  $labels = array(
    'name'                => _x( 'Case Studies', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Case Study', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Case Studies', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Case Study', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'project', 'ssi' ),
    'add_new_item'        => __( 'Add New Case Study', 'ssi' ),
    'new_item'            => __( 'New Case Study', 'ssi' ),
    'edit_item'           => __( 'Edit Case Study', 'ssi' ),
    'view_item'           => __( 'View Case Study', 'ssi' ),
    'all_items'           => __( 'All Case Studies', 'ssi' ),
    'search_items'        => __( 'Search Case Studies', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Case Studies:', 'ssi' ),
    'not_found'           => __( 'No case studies found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No case studies found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'case-study' ),
    'has_archive'         => __( 'case-studies', 'ssi' ),
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-money'
  );
  register_post_type( 'casestudy', $args );
}
