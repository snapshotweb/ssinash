<?php
add_action( 'init', 'ssi_post_type_career' );

function ssi_post_type_career() {

  $labels = array(
    'name'                => _x( 'Careers', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Career', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Careers', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Career', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'career', 'ssi' ),
    'add_new_item'        => __( 'Add New Career', 'ssi' ),
    'new_item'            => __( 'New Career', 'ssi' ),
    'edit_item'           => __( 'Edit Career', 'ssi' ),
    'view_item'           => __( 'View Career', 'ssi' ),
    'all_items'           => __( 'All Careers', 'ssi' ),
    'search_items'        => __( 'Search Careers', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Careers:', 'ssi' ),
    'not_found'           => __( 'No careers found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No careers found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'career' ),
    'has_archive'         => false,
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor' ),
    'menu_icon'           => 'dashicons-businessman'
  );
  register_post_type( 'career', $args );
}
