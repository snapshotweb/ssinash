<?php
add_action( 'init', 'ssi_post_type_client' );

function ssi_post_type_client() {

  $labels = array(
    'name'                => _x( 'Clients', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Client', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Clients', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Client', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'client', 'ssi' ),
    'add_new_item'        => __( 'Add New Client', 'ssi' ),
    'new_item'            => __( 'New Client', 'ssi' ),
    'edit_item'           => __( 'Edit Client', 'ssi' ),
    'view_item'           => __( 'View Client', 'ssi' ),
    'all_items'           => __( 'All Clients', 'ssi' ),
    'search_items'        => __( 'Search Clients', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Clients:', 'ssi' ),
    'not_found'           => __( 'No clients found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No clients found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'client' ),
    'has_archive'         => 'clients',
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-share-alt'
  );
  register_post_type( 'client', $args );
}
