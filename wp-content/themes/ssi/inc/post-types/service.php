<?php
add_action( 'init', 'ssi_post_type_service' );

function ssi_post_type_service() {

  $labels = array(
    'name'                => _x( 'Services', 'post type general name', 'ssi' ),
    'singular_name'       => _x( 'Service', 'post type singular name', 'ssi' ),
    'menu_name'           => _x( 'Services', 'admin menu', 'ssi' ),
    'name_admin_bar'      => _x( 'Service', 'add new on admin bar', 'ssi' ),
    'add_new'             => _x( 'Add New', 'service', 'ssi' ),
    'add_new_item'        => __( 'Add New Service', 'ssi' ),
    'new_item'            => __( 'New Service', 'ssi' ),
    'edit_item'           => __( 'Edit Service', 'ssi' ),
    'view_item'           => __( 'View Service', 'ssi' ),
    'all_items'           => __( 'All Services', 'ssi' ),
    'search_items'        => __( 'Search Services', 'ssi' ),
    'parent_item_colon'   => __( 'Parent Services:', 'ssi' ),
    'not_found'           => __( 'No services found.', 'ssi' ),
    'not_found_in_trash'  => __( 'No services found in Trash.', 'ssi' )
  );
  $args = array(
    'labels'              => $labels,
    'description'         => __( 'Description.', 'ssi' ),
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'service' ),
    'has_archive'         => false,
    'hierarchical'        => false,
    'menu_position'       => null,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon'           => 'dashicons-admin-tools'
  );
  register_post_type( 'service', $args );
}
