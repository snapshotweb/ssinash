<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package SnapShot_Interactive
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ssi_body_classes( $classes ) {

  // Adds a class of group-blog to blogs with more than 1 published author.
  if ( is_multi_author() ) {
    $classes[] = 'group-blog';
  }

  // Adds a class of hfeed to non-singular pages.
  if ( ! is_singular() ) {
    $classes[] = 'hfeed';
  }

  return $classes;
}
add_filter( 'body_class', 'ssi_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function ssi_pingback_header() {

  if ( is_singular() && pings_open() ) {
    echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
  }
}
add_action( 'wp_head', 'ssi_pingback_header' );

/**
 * Set up classes for 'Behind the Scenes' grid items.
 */
function ssi_grid_item_classes( $block_style, $background, $i, $classes = null ) {

  $grid_classes = 'grid-item';

  // Set grid item size based on position in the loop
  if ( $i == 4 || $i == 10 ) {
    $grid_classes .= ' grid-item--width2';
  } elseif ( $i == 5 ) {
    $grid_classes .= ' grid-item--width3 grid-item--height2';
  }
  // Add class for background style
  if ( $background != null ) {
    $grid_classes .= ' grid-item--' . $background;
  }
  // Add pattern class to black background
  if ( $background == 'black' ) {
    $grid_classes .= ' background-pattern';
  }
  if ( $block_style == 'image' ) {
    $grid_classes .= ' grid-item--image';
  }

  $grid_classes = 'class="' . $grid_classes . '"';
  echo $grid_classes;
}

/**
 * Set up classes for 'Behind the Scenes' grid item content blocks.
 */
function ssi_grid_content_classes( $block_style, $position = 'default', $classes = null ) {

  $content_classes = 'grid-item__content';

  // Add class for block style
  if ( $block_style != null ) {
    $content_classes .= ' grid-item__content--' . $content_classes;
  }

  // Add class for position in relation to the icon
  if ( $position != 'default' ) {
    $content_classes .= ' grid-item__content--position-' . $position;
  }

  $content_classes = 'class="' . $content_classes . '"';
  echo $content_classes;
}

/**
 * Show icons for 'Behind the Scenes' grid items.
 */
function ssi_grid_icon( $block_style, $month = '01', $day = '01' ) {

  // Variables
  $date_string      = $month . '-' . $day . '-2017';
  $date             = DateTime::createFromFormat( 'd-m-Y', $date_string );
  $month_full       = $date->format('F');
  $month_full       = __( (string)$month_full, 'ssi' );
  $month_short      = substr( $month_full, 0, 3 );
  $day_short        = $date->format('j');

  $icon_classes = 'grid-item__icon';

  if ( in_array( $block_style, [ 'tweet', 'instagram', 'facebook' ] ) ) {
    $icon_classes .= ' grid-item__icon--social';
    $icon_classes .= ' grid-item__icon--' . $block_style;
    $inner = null;
  } elseif ( $block_style == 'blog' ) {
    $icon_classes .= ' grid-item__icon--date';

    $inner_wrapper_open  = '<div class="content-wrapper grid-item__icon__content-wrapper"><div class="grid-item__icon__content">';
    $inner_wrapper_close = '</div></div>';
    $month_string        = '<span class="grid-item__icon--date__month">' . $month_short . '</span>';
    $day_string          = '<span class="grid-item__icon--date__day">' . $day_short . '</span>';

    $inner = $inner_wrapper_open . $month_string . $day_string . $inner_wrapper_close;
  }

  $wrapper_open  = '<div class="' . $icon_classes . '">';
  $wrapper_close = '</div>';

  echo $wrapper_open . $inner . $wrapper_close;
}

/**
 * Create links for 'Behind the Scenes' grid items.
 */
function ssi_grid_item_open_link( $link_href = null, $link_target = null, $link_data = null ) {

  if ( $link_href != null ) {
    $link_href = ' href="' . $link_href . '"';
    if ( $link_target != null ) {
      $link_target = ' target="' . $link_target . '"';
    }
    if ( $link_data != null ) {
      $link_data = ' ' . $link_data;
    }
    $link_tag = '<a' . $link_href . $link_target . $link_data . '>';
  } else {
    $link_tag = null;
  }

  return $link_tag;
}

/**
 * Disable all comments from all content
 */

// Disable support for comments and trackbacks in post types
function ssi_disable_comments_post_types_support() {

  $post_types = get_post_types();
  foreach ( $post_types as $post_type ) {
    if ( post_type_supports( $post_type, 'comments' ) ) {
      remove_post_type_support( $post_type, 'comments' );
      remove_post_type_support( $post_type, 'trackbacks' );
    }
  }
}
add_action( 'admin_init', 'ssi_disable_comments_post_types_support' );

// Close comments on the front-end
function ssi_disable_comments_status() {
  return false;
}
add_filter( 'comments_open', 'ssi_disable_comments_status', 20, 2 );
add_filter( 'pings_open', 'ssi_disable_comments_status', 20, 2 );

// Hide existing comments
function ssi_disable_comments_hide_existing_comments( $comments ) {
  $comments = array();
  return $comments;
}
add_filter( 'comments_array', 'ssi_disable_comments_hide_existing_comments', 10, 2 );

// Remove comments page in menu
function ssi_disable_comments_admin_menu() {
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'ssi_disable_comments_admin_menu' );

// Redirect any user trying to access comments page
function ssi_disable_comments_admin_menu_redirect() {
  global $pagenow;
  if ( $pagenow === 'edit-comments.php' ) {
    wp_redirect( admin_url() );
    exit;
  }
}
add_action( 'admin_init', 'ssi_disable_comments_admin_menu_redirect' );

// Remove comments metabox from dashboard
function ssi_disable_comments_dashboard() {
  remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'ssi_disable_comments_dashboard' );

// Remove comments links from admin bar
function ssi_disable_comments_admin_bar() {
  if ( is_admin_bar_showing() ) {
    remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
  }
}
add_action( 'init', 'ssi_disable_comments_admin_bar' );

/**
 * Function to retrieve location address info.
 */
function ssi_location_address( $detail = 'address', $location = 'Nashville' ) {

  $args = array(
    'post_type'       => 'location',
    'posts_per_page'  => 1,
    'title'           => $location,
  );
  $locations = new WP_Query( $args );
  while ( $locations->have_posts() ) {
    $locations->the_post();

    $location_object = get_field('address');
    $location_object = explode( ',', $location_object['address'] );

    if ( $detail == 'address' ) {
      $location_detail = $location_object[0];
    } elseif ( $detail == 'suite' ) {
      $location_detail = get_field('suite_number');
    } elseif ( $detail == 'city' ) {
      $location_detail = $location_object[1];
    } elseif ( $detail == 'state' ) {
      $location_detail = $location_object[2];
    } elseif ( $detail == 'zip' ) {
      $location_detail = get_field('zip');
    } elseif ( $detail == 'name' ) {
      $location_detail = get_the_title();
    } elseif ( $detail == 'phone' ) {
      $location_detail = get_field('phone');
    }
    wp_reset_postdata();

    return esc_html($location_detail);
  }
}

/**
 * Load AdRoll Pixel in the footer
 */
function ssi_adroll_pixel_output() {
  ?>
    <script type="text/javascript">
    adroll_adv_id = "R3OHSPWZUZCMRFKT7ZTAUS";
    adroll_pix_id = "PPIXNLOH3ZC7RCMD42BQKG";
    /* OPTIONAL: provide email to improve user identification */
    /* adroll_email = "username@example.com"; */
    (function () {
      var _onload = function() {
        if ( document.readyState && !/loaded|complete/.test(document.readyState) ) {
          setTimeout( _onload, 10 );
          return
        }
        if ( ! window.__adroll_loaded ) {
          __adroll_loaded = true;
          setTimeout( _onload, 50 );
          return
        }
        var scr = document.createElement("script");
        var host = ( ("https:" == document.location.protocol ) ? "https://s.adroll.com" : "http://a.adroll.com");
        scr.setAttribute( 'async', 'true' );
        scr.type = "text/javascript";
        scr.src = host + "/j/roundtrip.js";
        ( (document.getElementsByTagName('head') || [null])[0] || document.getElementsByTagName('script')[0].parentNode ).appendChild(scr);
      };
      if (window.addEventListener) {
        window.addEventListener( 'load', _onload, false );
      } else {
        window.attachEvent( 'onload', _onload )
      }
    }());
    </script>
  <?php
}

add_action( 'wp_footer', 'ssi_adroll_pixel_output', 99999 );
