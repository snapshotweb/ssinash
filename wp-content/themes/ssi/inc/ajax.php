<?php
add_action( 'wp_ajax_nopriv_ssi_more_post_ajax', 'ssi_more_post_ajax' );
add_action( 'wp_ajax_ssi_more_post_ajax', 'ssi_more_post_ajax' );

if ( ! function_exists('ssi_more_post_ajax') ) {

  function ssi_more_post_ajax() {

    $ppp     = ( isset( $_POST['ppp'] )) ? $_POST['ppp'] : 9;
    $cat     = ( isset( $_POST['cat'] ) ) ? $_POST['cat'] : 0;
    $offset  = ( isset( $_POST['offset'] ) ) ? $_POST['offset'] : 0;

    $content_type = is_search() ? 'search' : 'blog';

    $args = array(
      'post_type'       => 'post',
      'posts_per_page'  => $ppp,
      'cat'             => $cat,
      'offset'          => $offset,
    );

    $loop = new WP_Query($args);
    $out = '';

    if ( $loop -> have_posts() ) :
      while ( $loop -> have_posts() ) :
        $loop -> the_post();
        $category_out = array();
        $categories = get_the_category();

        foreach ( $categories as $category_one ) {
          $category_out[] ='<a href="' . esc_url( get_category_link( $category_one->term_id ) ) . '" class="' . strtolower( $category_one->name ) . '">' . $category_one->name . '</a>';
        }
        $category_out = implode( ', ', $category_out );

        $cat_out = ( ! empty( $categories ) ) ? '<span class="cat-links"><span class="screen-reader-text">' . esc_html__( 'Categories', 'ssi' ) . '</span>' . $category_out . '</span>' : '';

        $out .= locate_template( 'template-parts/content-' . $content_type . '.php', true, false );

      endwhile;
    endif;
    wp_reset_postdata();
    wp_die($out);
  }
}
