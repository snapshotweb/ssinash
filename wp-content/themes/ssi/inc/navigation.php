<?php
function ssi_nav_menus() {
  $locations = array(
    'main'        => esc_html__( 'Main Menu', 'ssi' ),
    'footer'      => esc_html__( 'Footer Menu', 'ssi' ),
    'secondary'   => esc_html__( 'Secondary Menu', 'ssi' ),
  );
  register_nav_menus( $locations );
}
add_action( 'init', 'ssi_nav_menus' );

/**
 * Redirect single CPTs to homepage as needed
 */
add_action( 'template_redirect', 'ssi_redirect_post' );

function ssi_redirect_post() {
  // List of post types to redirect
  $disabled_post_types = array( 'client', 'casestudy', 'career', 'location', 'testimonial' );

  // Get post type slug
  $queried_post_type   = get_query_var('post_type');

  if ( is_single() && in_array( $queried_post_type, $disabled_post_types ) ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}
