<?php
/**
 * Section layout options
 */
function ssi_section_layout_options() {

  $add_border      = get_sub_field('add_border');
  $border_top      = get_sub_field('border_top');
  $border_bottom   = get_sub_field('border_bottom');
  $border_color    = get_sub_field('border_color');
  $background      = get_sub_field('background_image');
  $dark_background = get_sub_field('dark_background');

  if ( get_sub_field('use_section_padding') == true ) {
    echo ' section--padded';
    if ( get_sub_field('expand_padding') == true ) {
      if ( get_sub_field('super_padding') == true ) {
        if ( get_sub_field('max_padding') == true ) {
          echo '--max';
        }
        else {
          echo '--super';
        }
      }
      else {
        echo '--extra';
      }
    }
  }
  if ( $add_border == true && $border_color ) {
    echo ' border--' . esc_html( $border_color );
  }
  if ( get_sub_field('dark_background') == true ) {
    echo ' section--light-text';
  }
  if ( get_sub_field('background_color') ) {
    echo ' section--' . esc_html( get_sub_field('background_color') );
  }
  if ( $background || $add_border == true ) {
    echo '" style="';

    if ( $background ) {
      echo 'background-image: url(\'' . esc_url( $background['url'] ) . '\');';
    }
    if ( $border_top == true && $add_border == true ) {
      echo 'border-top: 1px solid;';
    }
    if ( $border_bottom == true && $add_border == true ) {
      echo 'border-bottom: 1px solid;';
    }
  }
}

/**
 * Shortcode to display phone numbers in landing page footer CTA
 */
function ssi_footer_cta_phone() {
  ob_start();
  get_template_part( 'template-parts/footer/cta', 'phone' );
  return ob_get_clean();
}
add_shortcode( 'cta_phone', 'ssi_footer_cta_phone' );

/**
 * Retrieve location details from ACF fields
 */
function ssi_get_location_info( $detail = 'address', $echo = true ) {

  // Allowed input values
  $details = array( 'address', 'suite_number', 'city', 'state', 'zip', 'lat', 'lng' );

  if ( ! in_array( $detail, $details ) ) {
    return;
  }

  $location = get_field('address');
  $location = explode( ',', $location['address'] );

  if ( $detail == 'address' ) {
    $output = $location[0];
  } elseif ( $detail == 'city' ) {
    $output = $location[1];
  } elseif ( $detail == 'state' ) {
    $output = $location[2];
  } elseif ( $detail == 'lat' ) {
    $output = $location['lat'];
  } elseif ( $detail == 'lng' ) {
    $output = $location['lng'];
  } else {
    $output = get_field($detail);
  }

  if ( $echo === false ) {
    return esc_attr($output);
  } else {
    echo esc_attr($output);
  }
}
