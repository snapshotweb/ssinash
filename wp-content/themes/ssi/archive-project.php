<?php
/**
 * The template for displaying archive page for the project post type
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();

while ( have_posts() ) {
  the_post();
}

wp_reset_postdata();

get_template_part( 'template-parts/project-grid/nav', 'projects' );

get_template_part( 'template-parts/project-grid/section', 'project-grid' );

get_footer();
