<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SnapShot_Interactive
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>

<body <?php body_class(); ?>>
<div id="page" class="site<?php if ( $header_style != 'hero' ) { echo ' no-hero'; } ?>">
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ssi' ); ?></a>

  <?php
  if ( ! is_page_template( 'page-templates/page-landing-page.php' ) ) {
    get_template_part( 'template-parts/header/masthead' );
  }
  ?>

  <div id="content"
       class="site__content<?php if ( $header_style == 'none' || $header_style == null ) { echo ' site__content--no-header'; } ?>">

    <?php get_template_part( 'template-parts/header/page' ); ?>
