<?php
/**
 * The template for displaying the service archive page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

  <div class="row collapse expanded" data-equalizer data-equalize-on="medium">
    <div class="columns medium-3 large-4">

    </div>
    <div class="columns medium-9 large-8">

      <div class="row small-up-2 medium-up-3 large-up-4 xxlarge-up-5 collapse expanded">

        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();
          get_template_part( 'template-parts/service-grid/post', 'service' );
        endwhile;
        ?>

      </div><!-- .row -->
    </div><!-- .columns -->
  </div>

<?php endif; ?>

<?php
get_sidebar();
get_footer();
