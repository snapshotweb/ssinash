<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SnapShot_Interactive
 */

get_header(); ?>

<section class="single-blog section--padded">

  <div class="row">
    <div class="columns large-10 large-centered">

      <?php
      while ( have_posts() ) {
        the_post();
        get_template_part( 'template-parts/content', 'post' );
        // the_post_navigation();

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) {
          comments_template();
        }
      }
      // get_sidebar();
      ?>

    </div><!-- .columns -->
  </div><!-- .row -->

</section>

<?php
get_footer();
