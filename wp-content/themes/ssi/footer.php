<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SnapShot_Interactive
 */

?>

  </div><!-- #content -->

  <?php
  if ( get_field('add_cta_above_footer') == true && ! post_password_required() ) {
    get_template_part( 'template-parts/footer/cta' );
  }
  ?>

  <footer id="colophon" class="site__footer" role="contentinfo">

    <section class="site__footer__top">
      <div class="row">
        <div class="columns small-12">
          <div class="site__footer__navigation">
            <?php wp_nav_menu( array(
              'theme_location' => 'footer'
            )); ?>
          </div><!-- .site-info -->
        </div><!-- .columns -->
      </div><!-- .row -->
    </section>

    <section class="site__footer__bottom">
      <div class="row">
        <div class="columns small-12">
          <ul class="site__footer__info">
            <li class="site__footer__info__item"><?php printf( esc_html__( '%1$s Video & Website Design by %2$s', 'ssi' ), ssi_location_address('name'), '<span>' . get_bloginfo( 'name' ) . '</span>' ); ?></li>
            <li class="site__footer__info__item"><?php echo ssi_location_address(); echo ssi_location_address('suite') ? ', ' . ssi_location_address('suite') : null ?></li>
            <li class="site__footer__info__item"><?php echo trim( ssi_location_address('city') . ', ' . ssi_location_address('state') . ' ' . ssi_location_address('zip') ); ?></li>
            <li class="site__footer__info__item"><?php echo '<span>' . ssi_location_address('phone') . '</span>'; ?></li>
            <li class="site__footer__info__item"><a href="/privacy-policy"><span><?php _e( 'Privacy Policy', 'ssi' ); ?></span></a></li>
          </ul>
        </div><!-- .columns -->
      </div><!-- .row -->
    </section>
  </footer><!-- #colophon -->
</div><!-- #page -->

<?php /* get_template_part( 'template-parts/nav', 'menu' ); */ ?>

<?php wp_footer(); ?>

</body>
</html>
