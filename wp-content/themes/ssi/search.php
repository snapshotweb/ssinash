<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package SnapShot_Interactive
 */

get_header();
$cat_id = get_query_var('cat');
?>

<div class="blog__posts-list section--padded" role="ajax_posts">
  <div class="row" data-equalizer data-equalize-on="medium">
    <div class="columns medium-4 medium-push-8">
      <aside id="secondary" class="search__sidebar" role="complementary">
        <?php get_sidebar(); ?>
      </aside>
    </div>
    <div class="columns medium-8 medium-pull-4">
      <main id="main" class="search__main" role="main">

        <?php
        if ( have_posts() ) :
          $blog_post_count = 0;
          global $blog_post_count;
          ?>

          <div class="row medium-up-2">

            <?php
            while ( have_posts() ) :
              the_post();
              $blog_post_count++;
              get_template_part( 'template-parts/content', 'blog' );
            endwhile;
            ?>

            <div class="row medium-up-2 blog__ajax-posts"></div>
            <button class="blog__more-posts" data-category="<?php echo esc_attr( $cat_id ); ?>"><?php esc_html_e( 'Load More', 'ssi' ); ?></button>

          </div>

        <?php else : ?>
          <?php get_template_part( 'template-parts/content', 'none' ); ?>
        <?php endif; ?>

      </main>
    </div><!-- .columns -->
  </div>
</div>

<?php // the_posts_navigation(); ?>

<?php get_footer();
