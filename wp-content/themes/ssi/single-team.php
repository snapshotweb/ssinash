<?php
/**
 * The template for displaying all single posts for the team post type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SnapShot_Interactive
 */

get_header();
get_footer();
