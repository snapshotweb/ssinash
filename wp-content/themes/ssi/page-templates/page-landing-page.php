<?php
/**
 * Template Name: Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
if ( have_posts() ) {
  while ( have_posts()) {
    the_post();
    echo '<div class="row extra-margin">';
    echo '<div class="small-12 medium-6 large-7 columns">';
    echo '<section class="landing-page__content section--padded--extra">';
    the_content();
    echo '</section></div></div>';
    if ( get_field('show_team_bio') == true && get_field('team_bio') ) {
      get_template_part( 'template-parts/section', 'team-bio' );
    }
  }
}
wp_reset_postdata();
get_footer();
