<?php
/**
 * Template Name: Careers
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
while ( have_posts() ) {
  the_post();
  get_template_part( 'template-parts/content', 'page' );
  get_template_part( 'template-parts/page/careers/section', 'careers' );
}
get_footer();
