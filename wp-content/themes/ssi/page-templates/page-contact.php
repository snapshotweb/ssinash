<?php
/**
 * Template Name: Contact
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
if ( have_posts() ) {
  while ( have_posts()) {
    the_post();
    get_template_part( 'template-parts/page/contact/content' );
  }
}
wp_reset_postdata();
get_footer();
