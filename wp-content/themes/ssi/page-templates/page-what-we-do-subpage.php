<?php
/**
 * Template Name: What We Do Subpage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
if ( have_posts() ) {
  while ( have_posts()) {
    the_post();
    if ( get_field('show_project_gird') == true ) {
      get_template_part( 'template-parts/project-grid/section', 'project-grid' );
    }
    get_template_part( 'template-parts/content', 'page' );
  }
}
wp_reset_postdata();
get_footer();
