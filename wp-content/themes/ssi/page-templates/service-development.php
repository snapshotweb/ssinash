<?php
/**
 * Template Name: Development
 * Template Post Type: service
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

 get_header();

   while ( have_posts() ) {

     the_post();

     if ( get_field('show_project_grid') == true ) {
       get_template_part( 'template-parts/project-grid/section', 'project-grid' );
     }
     get_template_part( 'template-parts/content', 'page' );
   }

 get_footer();
