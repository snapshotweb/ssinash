<?php
/**
 * Template Name: Home
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <section class="section--padded--extra section--white section--gray-border">
    <div class="row">
      <div class="columns medium-5 large-4">
        <h2 class="section-heading--stylized"><?php _e( strip_tags( get_field('intro_copy_left'), '<strong>' ), 'ssi' ); ?></h2>
      </div>
      <div class="columns medium-7 large-8">
        <?php _e( get_field('intro_copy_right'), 'ssi' ); ?>
      </div>
    </div>
  </section>

  <?php
  get_template_part( 'template-parts/page/home/section', 'service-icons' );
  get_template_part( 'template-parts/page/home/section', 'recent-work' );
  get_template_part( 'template-parts/page/home/section', 'behind-the-scenes' );
  ?>

<?php
endwhile; endif; wp_reset_postdata();
get_footer();
