<?php
/**
 * The template for displaying all single posts for project post type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SnapShot_Interactive
 */

get_header();
while ( have_posts() ) {

  the_post();

  $hide_similar_projects = false;
  $hide_content = false;
  if ( have_rows('content_blocks') ) {
    while ( have_rows('content_blocks') ) {
      the_row();
      if ( get_row_layout() == 'similar_projects' ) {
        $hide_similar_projects = true;
      }
      if ( get_row_layout() == 'project_description' ) {
        $hide_content = true;
      }
    }
  }

  if ( $hide_content == false ) {
    get_template_part( 'template-parts/content', 'project' );
  }

  get_template_part( 'template-parts/section', 'content-blocks' );

  if ( $hide_similar_projects == false ) {
    get_template_part( 'template-parts/section', 'similar-projects' );
  }
}
get_footer();