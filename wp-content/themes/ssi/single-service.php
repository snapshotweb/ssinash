<?php
/**
 * The template for displaying all single posts for service post type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SnapShot_Interactive
 */

get_header();

  while ( have_posts() ) :

    the_post();

    if ( get_field('show_project_grid') == true ) {

      get_template_part( 'template-parts/project-grid/section', 'project-grid' );

    }

    get_template_part( 'template-parts/content', 'page' );

    $button = esc_html__( 'See Our Other Services', 'ssi' );
    $button = '<a href="' . get_page_link(30) . '" class="button button--large no-margin">' . $button . '</a>';
    echo '<section class="service-button section--white section--padded text-center">' . $button . '</section>';

  endwhile;

get_footer();
