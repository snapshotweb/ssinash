<?php
/**
 * SnapShot Interactive functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SnapShot_Interactive
 */

/**
 * Load classes
 */
require get_template_directory() . '/inc/classes.php';

if ( ! function_exists( 'ssi_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ssi_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   * If you're building a theme based on SnapShot Interactive, use a find and replace
   * to change 'ssi' to the name of your theme in all the template files.
   */
  load_theme_textdomain( 'ssi', get_template_directory() . '/languages' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support( 'post-thumbnails' );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

  // Set up the WordPress core custom background feature.
  add_theme_support( 'custom-background', apply_filters( 'ssi_custom_background_args', array(
    'default-color' => 'ffffff',
    'default-image' => '',
  ) ) );

  // Add theme support for selective refresh for widgets.
  add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ssi_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ssi_content_width() {
  $GLOBALS['content_width'] = apply_filters( 'ssi_content_width', 1400 );
}
add_action( 'after_setup_theme', 'ssi_content_width', 0 );

/**
 * Add ACF theme options
 */
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page( array(
    'page_title'    => __( 'Options', 'ssi' ),
    'menu_title'    => __( 'Global Options', 'ssi' ),
    'menu_slug'     => 'theme-general-settings',
    'capability'    => 'edit_posts',
    'redirect'      => false
  ) );
}

/**
 * Google Maps API key for ACF
 */
function ssi_acf_init() {
  acf_update_setting('google_api_key', 'AIzaSyBszD2qCGKPtOq3dUpQPQoqoT5-O_FBzTY');
}
add_action( 'acf/init', 'ssi_acf_init' );

/**
 * Add dynamic select options to job application form
 */
function ssi_add_position_titles_cf7 ( $tag, $unused ) {

  if ( $tag['name'] != 'open-positions' ) {
    return $tag;
  }

  $args = array ( 'post_type' => 'career',
    'numberposts' => 50,
    'orderby'     => 'title',
    'order'       => 'ASC' );
  $open_positions = get_posts($args);

  if ( ! $open_positions ) {
    return $tag;
  }

  // Get the existing WPCF7_Pipe objects
  $befores = $tag['pipes']->collect_befores();
  $afters  = $tag['pipes']->collect_afters();

  // Add the existing WPCF7_Pipe objects to the new pipes array
  $pipes_new = array();
  for ( $i=0; $i < count($befores); $i++ ) {
    $pipes_new[] = $befores[$i] . '|' . $afters[$i];
  }

  foreach ( $open_positions as $position ) {
    $tag['raw_values'][]   = $position->post_title;
    $tag['values'][]       = $position->post_title;
    $tag['labels'][]       = $position->post_title;
    $pipes_new[]           = $position->post_title . '|' . $position->post_title;
  }

  return $tag;
}
add_filter( 'wpcf7_form_tag', 'ssi_add_position_titles_cf7', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Register custom post types.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Register navigation menus.
 */
require get_template_directory() . '/inc/navigation.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue-scripts.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Layout functions.
 */
require get_template_directory() . '/inc/layout.php';

/**
 * Ajax functions.
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';
