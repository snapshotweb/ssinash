<?php
global $blog_post_count;
if ( get_the_post_thumbnail() ) {
  $thumb = get_the_post_thumbnail_url();
} else {
  $thumb = get_template_directory_uri() . '/assets/img/placeholder_blog.jpg';
}
// Set variables for article attributes
$post_class = 'blog__post';
$thumb_class = 'blog__post__thumb';
$info_class = 'blog__post__info';
$title_class = 'blog__post__title';
$meta_class = 'blog__post__posted-on entry-meta';
$post_open = '<div class="column">';
$post_close = '</div>';
$excerpt = null;
// Set variables for first article attributes
if ( $blog_post_count == 1 ) {
  $post_open = '</div>'; // close the loop's block grid
  $post_open .= '<div class="row"><div class="columns small-12 blog__post--featured__column">';
  $post_close = '</div></div>';
  $post_close .= '<div class="row medium-up-2">'; // re-open block grid
  $post_class .= ' blog__post--featured';
  $thumb_class .= ' blog__post--featured__thumb';
  $info_class .= ' blog__post--featured__info';
  $title_class .= ' blog__post--featured__title';
  $meta_class .= ' blog__post--featured__posted-on';
  $excerpt = '<div class="entry-summary">' . get_the_excerpt() . '</div>';
} elseif ( is_search() || is_404() ) {
  $info_data = ' data-equalizer-watch';
} else {
  $info_data = ' data-equalizer-watch';
} ?>

<?php echo $post_open; ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
    <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
      <div class="<?php echo $thumb_class; ?>" style="background-image: url('<?php echo esc_url($thumb); ?>');"></div>
    </a>
    <div class="<?php echo $info_class; ?>"<?php echo $info_data; ?>>
      <header class="entry-header">
        <?php the_title( sprintf( '<h3 class="%1$s"><a href="%2$s" rel="bookmark">', $title_class, esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        <p class="<?php echo $meta_class; ?>"><?php ssi_posted_on(); ?></p>
      </header>
      <?php echo $excerpt; ?>
    </div>
  </article>

<?php echo $post_close; ?>
