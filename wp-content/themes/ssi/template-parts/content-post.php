<?php $header_style = get_field('header_style'); ?>


<article id="post-<?php the_ID(); ?>" class="single-blog__post">

  <?php
  if ( $header_style != 'hero' ) :
    if ( get_field('hide_featured_image') != true ) : ?>

      <div class="single-blog__post__thumb">
        <?php the_post_thumbnail(); ?>
      </div>

    <?php
    endif;
  endif; ?>

  <div class="single-blog__post__content">
    <?php
    if ( $header_style == 'none' || $header_style == null ) {
      the_title( '<h1 class="entry-header single-blog__post__title">', '</h1>' );
      echo '<p class="single-blog__post__posted-on">';
      ssi_posted_on();
      echo '</p>';
    }
    ?>
    <div class="entry-content">
      <?php the_content();
      get_template_part( 'template-parts/section', 'content-blocks' );?>
    </div><!-- .entry-content -->
    <footer class="entry-footer single-blog__post__footer">

      <p><?php ssi_entry_footer(); ?></p>

    </footer>
  </div><!-- .single-blog__post-content -->

</article>

<nav class="single-blog__post-nav" role="navigation" data-equalizer data-equalize-on="medium">
  <div class="row medium-up-2">

    <?php
    $next_post = get_adjacent_post( false, '', false );
    $prev_post = get_adjacent_post( false, '', true );
    ?>

    <?php if ( $prev_post != null ) : ?>

      <div class="column single-blog__post-nav__col">
        <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
          <div class="prev-posts single-blog__post-nav__block" data-equalizer-watch>
            <div class="single-blog__post-nav__block__content">
              <span class="prev-link single-blog__post-nav__link"><?php _e( 'Previous Post', 'ssi' ); ?></span>
              <h2 class="single-blog__post-nav__post-title"><?php _e( get_the_title( $prev_post->ID ), 'ssi' ); ?></h2>
              <p class="single-blog__post-nav__post-content"><?php _e( wp_trim_words( $prev_post->post_content, 30, ' …' ), 'ssi' ); ?></p>
            </div>
          </div>
        </a>
      </div>

    <?php endif; if ( $next_post != null ) : ?>

      <div class="column single-blog__post-nav__col">
        <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
          <div class="prev-posts single-blog__post-nav__block" data-equalizer-watch>
            <div class="single-blog__post-nav__block__content">
              <span class="prev-link single-blog__post-nav__link"><?php _e( 'Next Post', 'ssi' ); ?></span>
              <h2 class="single-blog__post-nav__post-title"><?php _e( get_the_title( $next_post->ID ), 'ssi' ); ?></h2>
              <p class="single-blog__post-nav__post-content"><?php _e( wp_trim_words( $next_post->post_content, 30, ' …' ), 'ssi' ); ?></p>
            </div>
          </div>
        </a>
      </div>

    <?php endif; ?>

  </div>
</nav>
