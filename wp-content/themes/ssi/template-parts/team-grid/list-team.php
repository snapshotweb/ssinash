<section class="team<?php ssi_section_layout_options(); ?>">
  <?php
  $heading = get_sub_field('heading');
  if ( $heading ) {
    echo '<div class="row">';
    echo '<div class="columns small-12">';
    printf( esc_html__( '%1$s' . $heading . '%2$s', 'ssi' ), '<h2 class="team__section-title">', '</h2>' );
    echo '</div></div>';
  }

  $args = array(
    'post_type'       => 'team',
    'posts_per_page'  => -1,
  );
  // If only displaying select team members
  if ( get_sub_field('show_select_team_members') == true ) {
    // Get team members from post object field
    $team_members = get_sub_field('team_members');
    if ( $team_members ) {
      // Build array of team member IDs
      $team_ids = array();
      foreach ( $team_members as $post ) {
        setup_postdata( $post );
        $team_ids[] = get_the_ID();
      }
      wp_reset_postdata();

      // Add property to $args array to only show
      // select team members in the $team_ids array
      $args['post__in'] = $team_ids;

      // Callback to filter the ORDER BY part of the query
      function team_orderby_post_in( $orderby, $query ) {

        global $wpdb;

        // Remove it so it doesn't affect future queries
        remove_filter( current_filter(), __FUNCTION__ );

        $post__in = implode( ',', $query->get('post__in') );
        return "FIELD( {$wpdb->posts}.ID, $post__in )";
      }
      // Add filter
      add_filter( 'posts_orderby','team_orderby_post_in', 10, 2 );
    }
  }

  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) :

    // Build row classes
    $row_classes = 'team-grid row expanded collapse small-up-2 medium-up-3 large-up-4';
    if ( $the_query->post_count > 4 ) {
      $row_classes .= ' xlarge-up-5';
      if ( $the_query->post_count > 5 ) {
        $row_classes .= ' xlarge-up-6';
      }
    }
    ?>

    <div class="<?php echo $row_classes; ?>">

      <?php
      while ( $the_query->have_posts() ) :
        $the_query->the_post(); ?>

          <?php get_template_part( 'template-parts/team-grid/post', 'team' ); ?>

      <?php endwhile; ?>

    </div><!-- .row -->

    <?php
    $styles = '<style>';
    while ( $the_query->have_posts() ) : $the_query->the_post();
      if ( get_field('hover_image') ) :
        $name   = sanitize_title( get_the_title() );
        $bg_url = esc_url( get_field('hover_image')['url'] );
        /**
         * First we need to preload images in a hidden pseudo element
         * to prevent a glitchy hover state.
         *
         * @link http://particle-in-a-box.com/blog-post/pre-load-hover-images-css-only
         */
        $styles .= '.team__member--' . $name . ' .team__member__thumb::before';
        $styles .= '{';
        $styles .= 'content:url(\'' . $bg_url . '\');';
        // Hide the ::before element
        $styles .=  'width:0;height:0;visibility:hidden;';
        $styles .= '}';
        // Now set the background image on hover
        $styles .= '.team__member--' . $name . ' .team__member__thumb:hover';
        $styles .= '{';
        $styles .= 'background-image:url(\'' . $bg_url . '\')!important;';
        $styles .= '}';
      endif;
    endwhile;
    $styles .= '</style>';
    echo $styles;
    ?>

  <?php endif; wp_reset_postdata(); ?>

</section>
