<section class="team-nav section--padded">

  <div class="row expanded">
    <div class="small-12 columns">
      <div id="team-buttons" class="team-nav__team-category team-nav__button-group">

        <?php
        $args = array(
          'taxonomy'      => 'team-category',
          'parent'        => 0,
          'hide_empty'    => true
        );
        $terms = get_terms( $args );
        ?>

        <button class="team-nav__team-category__button is-checked" data-filter="*">View all</button>

        <?php foreach ( $terms as $term ) : ?>
          <button class="team-nav__team-category__button" data-filter="<?php echo '.' . $term->slug; ?>"><?php echo $term->name; ?></button>
        <?php endforeach; ?>

      </div>
    </div><!-- .columns -->
  </div><!-- .row -->

</section>
