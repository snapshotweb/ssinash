<?php
// Retrieve tax terms to apply class names for Isotope sorting
$taxonomy = array( 'team-category' );
$term_list = wp_get_object_terms( $post->ID, $taxonomy ); ?>

<div class="column team-grid__item<?php

// Add class names based on tax terms
if ( ! empty( $term_list ) && ! is_wp_error( $term_list ) ) {
  foreach( $term_list as $term ) {
    echo ' ' . $term->slug;
  }
}
?>">

<?php if ( get_the_content() != null ) : ?>

  <a href="<?php the_permalink(); ?>">

<?php endif; ?>

    <div class="team__member team__member--<?php echo sanitize_title( get_the_title() ); ?>">
      <div class="team__member__thumb"
           style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
        <div class="team__member__thumb__content">
          <h3 class="team__member__thumb__name"><?php the_title(); ?></h3>
          <h4 class="team__member__thumb__position"><?php esc_html_e( get_field('position'), 'ssi' ); ?></h4>
        </div>
      </div>
    </div>

<?php if ( get_the_content() != null ) : ?>

  </a>

<?php endif; ?>

</div><!-- .column -->
