<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="row">
    <div class="columns small-12 text-center section--padded--extra">
      <?php echo get_the_password_form(); ?>
    </div>
  </div>
</article>
