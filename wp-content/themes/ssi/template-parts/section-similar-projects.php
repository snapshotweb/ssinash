<section class="single-project__related section--padded--extra">
  <div class="row">
    <div class="columns small-12">
      <h2>Similar Projects</h2>
    </div>
  </div>
  <div class="row medium-up-3">

    <?php
    $taxonomy = 'project-type';
    // Get all terms for project type taxonomy and return term IDs
    $post_terms = wp_get_post_terms( $post->ID, $taxonomy, array( "fields" => "ids" ) );
    // Look for posts with similar tags as current post
    $tag_ids = array();
    foreach ( $tag_ids as $individual_tag ) {
      $tag_ids[] = $individual_tag->term_id;
    }
    $args = array(
      'post_type'         => 'project',
      'tag__in'           => $tag_ids,
      'post__not_in'      => array( $post->ID ),
      'posts_per_page'    => 3,
      'caller_get_posts'  => 1,
      // Tax query for projects in same project type as current post
      'tax_query'         => array(
        array(
          'taxonomy'      => $taxonomy,
          'field'         => 'term_id',
          'terms'         => $post_terms
        )
      )
    );
    $my_query = new WP_Query( $args );

    while ( $my_query->have_posts() ) {
      $my_query->the_post();
      get_template_part( 'template-parts/project-grid/post', 'project' );
    }
    wp_reset_postdata(); ?>

  </div>
</section>
