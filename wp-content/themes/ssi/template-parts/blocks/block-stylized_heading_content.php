<section class="page-header page-header--simple page-header--simple--<?php the_sub_field('text_orientation'); ?>">
  <div class="row">
    <?php get_template_part( 'template-parts/blocks/stylized-heading-content/simple', get_sub_field('text_orientation') ); ?>
  </div><!-- .row -->
</section>