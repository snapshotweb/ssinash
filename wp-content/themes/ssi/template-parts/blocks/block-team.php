<?php

if ( get_sub_field('hide_nav') != true ) {
  get_template_part( 'template-parts/team-grid/nav', 'team' );
}
get_template_part( 'template-parts/team-grid/list', 'team' );
