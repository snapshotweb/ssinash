<?php $background = get_sub_field('background_image'); ?>
<section class="layout--two-col<?php ssi_section_layout_options(); ?>">

  <div class="row">

    <div class="large-<?php
    if ( get_sub_field('restrict_to_columns') ) {
      the_sub_field('restrict_to_columns');
    }
    else {
      echo '12';
    } ?> columns large-centered">

      <div class="row">
        <div class="columns medium-6">
          <?php the_sub_field('left_column'); ?>
        </div>
        <div class="columns medium-6">
          <?php the_sub_field('right_column'); ?>
        </div>
      </div>

    </div>
  </div>

</section>