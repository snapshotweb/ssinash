<section class="icons section--padded--extra">
  <div class="row">
    <div class="columns small-12">
      <h2 class="icons__heading">
        <?php the_sub_field('section_heading'); ?>
      </h2>
    </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-<?php the_sub_field('columns_per_row'); ?>">
    <?php
    if ( have_rows ('icons') ) :
      while ( have_rows ('icons') ) :
        the_row();
        $icon = get_sub_field('icon'); ?>

        <div class="column icons__icon__column">
          <img src="<?php echo $icon['url']; ?>" class="icons__icon" />
          <h3 class="icons__icon__label"><?php the_sub_field('title'); ?></h3>
          <div class="icons__icon__description">
            <?php the_sub_field('description'); ?>
          </div>
        </div>

      <?php endwhile;
    endif; ?>
</section>
