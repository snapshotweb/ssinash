<?php $background = get_sub_field('background_image'); ?>
<section class="layout--one-col<?php ssi_section_layout_options(); ?>">

  <div class="row">

    <div class="large-<?php
    if ( get_sub_field('restrict_to_columns') ) {
      the_sub_field('restrict_to_columns');
    }
    else {
      echo '12';
    } ?> columns large-centered">
      <?php the_sub_field('content'); ?>
    </div>
  </div>

</section>