<section class="logo-grid">
  <?php
  $post_objects = get_sub_field('logo_grid');
  $heading = get_sub_field('section_heading');
  $logos = get_sub_field('logos');
  ?>

  <div class="row">
    <div class="columns small-12 small-centered">

      <?php if ( $heading ) : ?>
        <h2><?php echo $heading; ?></h2>
      <?php endif; ?>

      <div class="row collapse small-up-2 medium-up-3 large-up-4" data-equalizer data-equalize-on="small">

        <?php if ( $post_objects ) :
          foreach ( $post_objects as $post ) :
            setup_postdata($post); ?>

            <div class="column logo-grid__column" data-equalizer-watch>
              <div class="logo-grid__item">
                <?php the_post_thumbnail(); ?>
              </div>
            </div>

          <?php endforeach;
        wp_reset_postdata();

        elseif ( $logos ) :
          foreach( $logos as $logo ): ?>

            <div class="column logo-grid__column" data-equalizer-watch>
              <div class="logo-grid__item">
                <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
              </div>
            </div>

          <?php endforeach;
        endif; ?>

      </div>

    </div>
  </div>
</section>
