<header>
  <h2 class="section-heading--stylized">
    <?php
    $heading = get_sub_field('heading');
    _e( wp_kses( $heading, array( 'br' => array() ) ), 'ssi' );
    ?>
    <strong>
      <?php _e( get_sub_field('heading_emphasis'), 'ssi' ); ?>
    </strong>
  </h2>
</header>
