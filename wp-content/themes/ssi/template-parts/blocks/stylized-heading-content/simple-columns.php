<div class="columns medium-12 large-4">
  <?php get_template_part( 'template-parts/blocks/stylized-heading-content/content', 'heading' ); ?>
</div>
<div class="columns medium-7 large-8">
  <?php get_template_part( 'template-parts/blocks/stylized-heading-content/content', 'content' ); ?>
</div>