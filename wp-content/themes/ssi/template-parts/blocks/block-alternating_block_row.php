<?php if ( have_rows('alternating_blocks') ) : $i = 0; ?>

  <section class="alternating-blocks">

    <?php while ( have_rows('alternating_blocks') ) :
      $i++;
      the_row();
      $image = get_sub_field('image');
      $link_type = get_sub_field('link_type');
      $vimeo_id = esc_attr( get_sub_field( 'vimeo_id') );
      $page_link = esc_attr( get_sub_field( 'page_link' ) );
      $content_link_type = get_sub_field('content_link_type');
      $content_vimeo_id = esc_attr( get_sub_field( 'content_vimeo_id') );
      $content_page_link = esc_attr( get_sub_field( 'content_page_link' ) );
      // Set link attributes for image column
      if ( $link_type == 'vimeo' && $vimeo_id ) {
        $link_atts = ' href="//vimeo.com/' . $vimeo_id . '" data-lity';
      } elseif ( $link_type == 'internal' ) {
        $link_atts = ' href="' . $page_link . '"';
      } elseif ( $link_type == 'external' ) {
        $link_atts = ' href="' . $page_link . '" target="_blank"';
      } else {
        $link_atts = null;
      }
      // Set link attributes for content column
      if ( $content_link_type == 'vimeo' && $content_vimeo_id ) {
        $content_link_atts = ' href="//vimeo.com/' . $content_vimeo_id . '" data-lity';
      } elseif ( $content_link_type == 'internal' ) {
        $content_link_atts = ' href="' . $content_page_link . '"';
      } elseif ( $content_link_type == 'external' ) {
        $content_link_atts = ' href="' . $content_page_link . '" target="_blank"';
      } else {
        $content_link_atts = null;
      }
      ?>

      <div class="alternating-blocks__row row collapse" data-equalizer data-equalize-on="medium">

        <div class="alternating-blocks__column columns medium-6<?php if ( $i % 2 != 0 ) : ?> medium-push-6<?php endif; ?>">
          <?php echo $link_atts ? '<a' . $link_atts . '>' : null; ?>
            <div class="alternating-blocks__thumb"
                 style="background-image:url('<?php echo $image['url']; ?>');" data-equalizer-watch>
              <?php
              if ( $link_type == 'vimeo' && $vimeo_id ) {
                get_template_part( 'template-parts/icon', 'play' );
              } ?>
            </div>
          <?php echo $link_atts ? '</a>' : null; ?>
        </div>

        <div class="alternating-blocks__column columns medium-6<?php if ( $i % 2 != 0 ) : ?> medium-pull-6<?php endif; ?>">
          <?php echo $content_link_atts ? '<a' . $content_link_atts . '>' : null; ?>
            <div class="alternating-blocks__content" data-equalizer-watch>
              <?php the_sub_field('content'); ?>
            </div>
          <?php echo $content_link_atts ? '</a>' : null; ?>
        </div>

      </div>

    <?php endwhile; ?>

  </section>

<?php endif;
