<?php
$video_source = get_sub_field('video_source');
$video_id = esc_html( get_sub_field('video_id') );
if ( $video_source == 'vimeo' ) {
  $iframe = '<iframe src="https://player.vimeo.com/video/' . $video_id . '?title=0&amp;byline=0&amp;portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
}
elseif ( $video_source == 'youtube' ) {
  $iframe = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe>';
}
?>

<section class="video-embed">

  <div class="responsive-embed widescreen">
    <?php echo $iframe; ?>
  </div>

</section>
