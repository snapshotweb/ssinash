<?php $background = get_sub_field('background_image'); ?>
<section class="media-gallery<?php ssi_section_layout_options(); ?>">
  <?php $images = get_sub_field('gallery'); ?>
  <div class="row medium-up-2 large-up-<?php the_sub_field('columns_per_row'); ?>">
    <?php $rand_id = rand( 11, 55555 ); ?>
    <?php foreach ( $images as $image ) : ?>
      <div class="column">
        <div class="media-gallery__thumb">
          <a href="<?php echo esc_url( $image['url'] ); ?>" data-fancybox="group_<?php echo $rand_id; ?>" data-caption="<?php echo esc_attr( $image['caption'] ); ?>">
            <img class="media-gallery__thumb__img"
                 src="<?php echo esc_url( $image['sizes']['medium'] ); ?>" alt="<?php echo $image['alt']; ?>" />
          </a>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</section>
