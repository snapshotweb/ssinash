<?php
wp_reset_postdata();
$testimonials = get_sub_field('testimonials');
$background_color = get_sub_field('background_color');
if ( $testimonials ) : ?>

  <section class="single-project__testimonials section--dark-black section--padded--extra<?php echo get_sub_field('light_text') ? ' single-project__testimonials--light-text' : null; ?>"<?php if ( $background_color ) : ?>
           style="background-color: <?php echo esc_attr($background_color); ?>;"<?php endif; ?>>
    <div class="row">
      <div class="columns medium-8 large-6 medium-centered">
        <h2 class="screen-reader-text"><?php _e( 'Testimonials', 'ssi' ); ?></h2>
        <div class="single-project__testimonials__<?php echo count($testimonials) > 1 ? 'slider' : 'container'; ?>">
          <?php foreach ( $testimonials as $post ) : setup_postdata($post); ?>
            <div class="slick single-project__testimonials__item">
              <span class="single-project__testimonials__item__company"><?php echo esc_html( get_field('company') ); ?></span>
              <div class="single-project__testimonials__item__content">
                <?php the_content(); ?>
              </div>
              <?php $name = get_field('last_name') ? get_field('first_name') . ' ' . get_field('last_name') : get_field('first_name'); ?>
              <span class="single-project__testimonials__item__name"><?php echo esc_html($name); ?></span>
              <span class="single-project__testimonials__item__title"><?php echo esc_html( get_field('job_title') ); ?></span>
            </div>
          <?php endforeach; ?>
        </div>

      </div>
    </div>
  </section>

<?php endif; wp_reset_postdata(); ?>
