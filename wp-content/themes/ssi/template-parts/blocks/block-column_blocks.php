<?php $background = get_sub_field('background_image'); ?>
<section class="layout--column-blocks section--padded<?php ssi_section_layout_options(); ?>">

  <div class="row">

    <?php if ( get_sub_field('heading') ) : ?>

      <div class="columns small-12">
        <h2 class="layout--column-blocks__heading"><?php esc_html_e( get_sub_field('heading'), 'ssi' ); ?></h2>
      </div>

    <?php endif; ?>

    <div class="large-<?php
    if ( get_sub_field('restrict_to_columns') ) {
      echo esc_html( get_sub_field('restrict_to_columns') );
    }
    else {
      echo '12';
    } ?> columns large-centered">
      <div class="row medium-up-2 large-up-<?php echo esc_html( get_sub_field('columns_per_row') ); ?>">
        <?php $content_type = get_sub_field('content_type'); ?>

        <?php if ( have_rows('blocks') ) : while ( have_rows('blocks') ) : the_row(); ?>

          <div class="column">

            <?php if ( $content_type == 'editor' ) : ?>

              <?php the_sub_field('column_block'); ?>

            <?php elseif ( $content_type == 'image-links' ) : ?>

              <?php
              $image = get_sub_field('image');
              $link_type = get_sub_field('link_type');
              $target = 'null';
              if ( $link_type == 'internal' ) {
                $link = esc_url( get_sub_field('internal_link') );
              }
              elseif ( $link_type == 'external' ) {
                $link = esc_url( get_sub_field('external_link') );
                $target = ' target="_blank"';
              }
              elseif ( $link_type == 'video' ) {
                $link = '//vimeo.com/' . esc_url( get_sub_field('vimeo_id') );
                $target = ' data-lity';
              }
              $img = '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '" class="layout--column-blocks__image" />';
              ?>

              <div class="layout--column-blocks__image__container">

                <?php if ( $link_type != 'none' ) : ?>

                  <a href="<?php echo $link; ?>" class="layout--column-blocks__image__link"<?php echo $target; ?>>
                    <?php echo $img; ?>
                    <?php if ( $link_type  == 'video' ) : ?>
                      <i class="layout--column-blocks__image__play-icon icon icon--play"><span class="screen-reader-text"><?php _e( 'Play video', 'ssi' ); ?></span></i>
                    <?php endif; ?>
                  </a>

                <?php else : ?>

                  <?php echo $img; ?>

                <?php endif; ?>

              </div>

              <p class="layout--column-blocks__image__caption">
                <?php echo esc_html( get_sub_field('caption') ); ?>
              </p>

            <?php endif; ?>

          </div>

        <?php endwhile; endif; ?>

      </div>
    </div>
  </div>

</section>
