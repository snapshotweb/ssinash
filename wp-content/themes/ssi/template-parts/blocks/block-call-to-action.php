<?php
$cta = get_sub_field('call-to-action');
$target = get_sub_field('new_tab');
if ( $target == true ) {
  $target = '_blank';
}
else {
  $target = '_self';
}
if ( $cta ) :
  $post = $cta;
  setup_postdata( $post );
  $link = get_field('hyperlink');
  ?>

  <div class="call-to-action">
    <a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
      <?php the_post_thumbnail(); ?>
    </a>
  </div>

<?php
endif;
wp_reset_postdata();
