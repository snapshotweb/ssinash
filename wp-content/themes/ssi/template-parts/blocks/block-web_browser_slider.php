<section class="web-browser-slider<?php ssi_section_layout_options(); ?>">
  <div class="row">
    <div class="columns large-10 large-centered">
      <!-- begin rendering browser window -->
      <div class="web-browser-window">
        <div class="web-browser-window__header">
          <div class="web-browser-window__header__buttons">
            <div class="web-browser-window__header__button web-browser-window__header__button--red"></div>
            <div class="web-browser-window__header__button web-browser-window__header__button--yellow"></div>
            <div class="web-browser-window__header__button web-browser-window__header__button--green"></div>
          </div><!-- buttons -->
        </div><!-- header -->
        <div class="web-browser-window__container">
          <!-- begin content -->
          <?php $slides = get_sub_field('slides'); ?>
          <?php foreach ( $slides as $image ) : ?>
            <div class="web-browser-window__slide">
              <img src="<?php echo $image['url']; ?>" />
            </div>
          <?php endforeach; ?>
          <!-- end content -->
        </div><!-- container -->
      </div><!-- window -->
      <!-- begin rendering browser window -->
    </div>
  </div>
</section>
