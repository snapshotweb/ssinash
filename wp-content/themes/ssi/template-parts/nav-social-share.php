<ul class="social-share">
  <!-- Facebook -->
  <li>
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank">
      <i class="fa fa-facebook"></i>
    </a>
  </li>
  <!-- Twitter -->
  <li>
    <a href="https://twitter.com/home?status=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank">
      <i class="fa fa-twitter"></i>
    </a>
  </li>
  <!-- LinkedIn -->
  <li>
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( get_the_permalink() ); ?>&title=&summary=&source=" target="_blank">
      <i class="fa fa-linkedin"></i>
    </a>
  </li>
  <!-- Pinterest -->
  <li>
    <a href="https://pinterest.com/pin/create/button/?url=&media=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank">
      <i class="fa fa-pinterest"></i>
    </a>
  </li>
</ul>