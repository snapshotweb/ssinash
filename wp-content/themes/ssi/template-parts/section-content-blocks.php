<?php if ( have_rows('content_blocks') ) {
  while ( have_rows('content_blocks') ) {
    the_row();
    get_template_part( 'template-parts/blocks/block', get_row_layout() );
  }
} ?>