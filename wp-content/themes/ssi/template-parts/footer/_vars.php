<?php
$layout_style         = get_field('layout_style');
$background_style     = get_field('background_style');
$background_image     = get_field('background_image');
$background_image_url = $background_image['url'];
$background_color     = get_field('background_color');
$padding              = get_field('padding');
$text_color           = get_field('text_color');

$section_class_base   = 'site__footer__cta';
$section_class        = $section_class_base . ' ' . $section_class_base . '--' . $layout_style;


if ( $background_image ) {
  $background_css = ' style="';
  if ( $background_image ) {
    $background_css .= 'background-image:url(' . $background_image_url . ');';
  }
  $background_css .= '"';
  $section_class  .= ' ' . $section_class_base . '--bg-image';
}
else {
  $background_css = null;
}

if ( $background_color ) {
  $section_class .= ' ' . $section_class_base . '--bg-color';
  $section_class .= ' section--' . $background_color;
}

if ( $padding != 'none' ) {
  if ( $padding == 'extra' ) {
    $extra = '--extra';
  }
  else {
    $extra = null;
  }
  $section_class .= ' section--padded' . $extra;
}

if ( $text_color ) {
  $section_class .= ' ' . $section_class_base . '--text-' . $text_color;
}
