<?php
$cta = get_field('select_cta');
if ( $cta ) {
  $post = $cta;
  setup_postdata($post);

  get_template_part( 'template-parts/footer/cta', esc_attr( get_field('layout_style') ) );

  wp_reset_postdata();
}
