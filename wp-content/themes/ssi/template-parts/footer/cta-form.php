<?php include '_vars.php'; ?>

<section class="<?php echo $section_class; ?>"<?php echo $background_css; ?>>
  <div class="row">
    <div class="columns medium-6">
      <h3 class="<?php echo $section_class_base; ?>__heading heading-underline">
        <?php esc_html_e( get_field('heading'), 'ssi' ); ?>
      </h3>

      <?php if ( get_field('subheading') ) : ?>

        <div class="<?php echo $section_class_base; ?>__subheading">
          <?php the_field('subheading'); ?>
        </div>

      <?php endif; ?>

      <?php
      if ( get_field('show_phone_numbers') == true ) {
        get_template_part( 'template-parts/footer/cta', 'phone' );
      }
      ?>

    </div>
    <div class="<?php echo $section_class_base; ?>__form">
      <div class="columns medium-6">
        <?php
        $form_shortcode = get_field('form_shortcode');
        if ( get_field('form_shortcode') != null ) {
          // $form_shortcode = esc_html( get_field('form_shortcode') );
          echo do_shortcode( $form_shortcode );
        }
        ?>
      </div>
    </div>
  </div>
</section>
