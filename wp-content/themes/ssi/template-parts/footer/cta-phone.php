<?php include '_vars.php'; ?>

<div class="<?php echo $section_class_base; ?>__phone">
  <i class="<?php echo $section_class_base; ?>__phone__icon icon icon--phone--mobile" aria-hidden="true"></i>
  <div class="<?php echo $section_class_base; ?>__phone__content">

    <?php
    $query_args = array(
      'post_type'         => 'location',
      'posts_per_page'    => -1
    );
    $the_query = new WP_Query( $query_args );
    if ( $the_query->have_posts() ) :
      while ( $the_query->have_posts() ) :
        $the_query->the_post();
        ?>

        <hgroup class="<?php echo $section_class_base; ?>__phone__location">
          <?php the_title( '<h3 class="' . $section_class_base . '__phone__location__name">', ':</h3>' ); ?>
          <h4 class="<?php echo $section_class_base; ?>__phone__location__number"><?php esc_html_e( get_field('phone') ); ?></h4>
        </hgroup>

      <?php
      endwhile;
    endif;
    wp_reset_postdata();

    // Reset original post object loop
    $cta = get_field('select_cta');
    if ( $cta ) {
      $post = $cta;
      setup_postdata( $post );
    }
    ?>

  </div>
</div>
