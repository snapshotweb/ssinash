<?php include '_vars.php'; ?>

<section class="<?php echo $section_class; ?>"<?php echo $background_css; ?>>
  <div class="row">
    <div class="columns medium-10 large-8 xlarge-7 medium-centered">
      <h3 class="<?php echo $section_class_base; ?>__heading">
        <?php esc_html_e( get_field('heading'), 'ssi' ); ?>
      </h3>
      <?php if ( get_field('subheading') ) : ?>
        <div class="<?php echo $section_class_base; ?>__subheading">
          <?php the_field('subheading'); ?>
        </div>
      <?php endif; ?>

      <?php if ( $layout_style == 'button' && get_field('button_link') ) : ?>
        <a class="button button--large <?php echo $section_class_base; ?>__button"
           href="<?php the_field('button_link'); ?>"<?php
           echo get_field('new_tab') == true ? ' target="_blank"' : null; ?>>
          <?php
          if ( get_field('button_text') ) {
            esc_html_e( get_field('button_text'), 'ssi' );
          }
          else {
            esc_html_e( 'Contact Us', 'ssi' );
          }
          ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</section>
