<?php if ( have_rows('behind_the_scenes') ) : ?>

  <section id="social-feed" class="show-for-medium behind-the-scenes">

    <div class="grid">
      <div class="grid-item grid-item--white">
        <h2>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/heading_behind-the-scenes.svg" alt="" />
          <span class="screen-reader-text"><?php _e( 'Behind the Scenes', 'ssi' ); ?></span>
        </h2>
      </div>

      <?php
      $i = 0;
      while ( have_rows('behind_the_scenes') ) :
        the_row();
        // Begin counter to determine variant block sizes
        $i++;

        // Get proper format of post date for blog post blocks
        $blog_post = get_sub_field('blog_post');
        if ( $blog_post != null ) {
          $date  = DateTime::createFromFormat( 'Y-m-d H:i:s', $blog_post->post_date );
          $month = $date->format( 'm' );
          $day   = $date->format( 'd' );
        } else { // Return defaults to prevent breaking the function
          $month = '01';
          $day   = '01';
        }
        // Set block titles
        if ( get_sub_field('title') ) {
          $block_title = get_sub_field('title');
        } elseif ( get_sub_field('block_style') == 'blog' && $blog_post != null ) {
          $block_title = get_the_title( $blog_post->ID );
        } else {
          $block_title = null;
        }
        // Set block content
        if ( get_sub_field('block_style') == 'blog' && $blog_post != null ) {
          $block_content = '<div class="show-for-xxlarge">' . get_sub_field('content') . '</div>';
        } else {
          $block_content = get_sub_field('content');
        }
        // Get background image when appropriate
        $background_image = get_sub_field('background_image');
        if (
          ( $background_image && get_sub_field('background_style') == 'image' ) ||
          ( $background_image && get_sub_field('block_style') == 'image' )
        ) {
          $background_style = ' style="background-image:url(' . $background_image['url'] . ')"';
        } else { // Return default
          $background_style = null;
        }
        // Set icon position
        if ( get_sub_field('content_placement') ) {
          $position = get_sub_field('content_placement');
        } else {
          $position = 'default';
        }
        // Set hyperlinks
        if ( get_sub_field('block_style') == 'blog' ) {
          $link_href   = esc_url( get_permalink( get_sub_field('blog_post')->ID ) );
          $link_target = null;
          $link_data   = null;
        }
        elseif ( get_sub_field('link_type') == 'external' ) {
          $link_href   = esc_url( get_sub_field('external_link') );
          $link_target = '_blank';
          $link_data   = null;
        } elseif ( get_sub_field('link_type') == 'internal' ) {
          $link_href   = esc_attr( get_sub_field('internal_link') );
          $link_target = '_self';
          $link_data   = null;
        } elseif ( get_sub_field('link_type') == 'video' ) {
          $link_href   = '//vimeo.com/' . esc_attr( get_sub_field('vimeo_id') );
          $link_target = null;
          $link_data   = 'data-lity';
        } else {
          $link_href   = null;
          $link_target = null;
          $link_data   = null;
        }
        $open_link = ssi_grid_item_open_link( $link_href, $link_target, $link_data );
        ?>

        <?php echo $open_link; ?>

          <div <?php ssi_grid_item_classes( get_sub_field('block_style'), get_sub_field('background_style'), $i ); echo $background_style; ?>>
            <div <?php ssi_grid_content_classes( get_sub_field('block_style'), $position ); ?>>
              <?php if (
                $block_title &&
                get_sub_field('block_style') != 'tweet' &&
                get_sub_field('block_style') != 'facebook' &&
                get_sub_field('block_style') != 'instagram'
              ) : ?>
                <h4 class="grid-item__content__title"><?php esc_html_e( $block_title, 'ssi' ); ?></h4>
              <?php endif; ?>
              <?php echo $block_content; ?>
            </div>
            <?php ssi_grid_icon( get_sub_field('block_style'), $month, $day ); ?>
          </div>

        <?php echo ( $open_link ? '</a>' : null ); ?>

      <?php endwhile; ?>

    </div><!-- .grid -->

  </section>

<?php endif; ?>
