<?php
$recent_posts = get_field('recent_posts');
if ( $recent_posts ) : ?>
  <section class="section--padded--extra section--black section--pattern home__recent-work">
    <div class="row">
      <div class="columns medium-6">
        <h2 class="section-heading--stylized section-heading--stylized--larger"><?php _e( strip_tags( get_field('recent_work_heading'), '<strong>' ), 'ssi' ); ?></h2>
        <?php _e( get_field('recent_work_copy'), 'ssi' ); ?>
          <a class="button hollow gray" href="/projects"><?php _e( 'View All', 'ssi' ); ?></a>
        </p>
      </div>
      <div class="columns medium-6">

        <?php
        $i = 0;
        foreach ( $recent_posts as $post ) :
          setup_postdata($post);
          $i++;
          if ( $i == 1 ) : ?>

            <div class="row small-up-1">
              <?php get_template_part( 'template-parts/project-grid/post', 'project' ); ?>
            </div>
            <div class="row medium-up-3">

          <?php else :
            get_template_part( 'template-parts/project-grid/post', 'project' );
          endif;
        endforeach; wp_reset_postdata(); ?>

        </div><!-- .row (referenced inside loop) -->
      </div><!-- .columns -->
    </div><!-- .row -->
  </section>
<?php endif;
