<?php
if ( get_sub_field('hide_excerpts') ) {
  $hide_excerpts = get_sub_field('hide_excerpts');
}
if ( get_sub_field('heading') ) {
  $heading = get_sub_field('heading');
}
// Establish mobile column size depending on excerpts
if ( $hide_excerpts == true ) {
  $col = 'small-up-2';
}
else {
  $col = 'medium-up-2';
}

// Query service posts
$args = array(
  'post_type'         => 'service',
  'posts_per_page'    => -1
);
$services = new WP_Query( $args );
if ( $services->have_posts() ) : ?>

  <section class="section--padded--extra section--white service-icons section--gray-border">

    <?php if ( $heading ) : ?>

      <div class="row">
        <div class="columns small-12">
          <h2 class="service-icons__heading"><?php esc_html_e( $heading, 'ssi' ); ?></h2>
        </div>
      </div>

    <?php endif; ?>

    <div class="row <?php echo $col; ?> large-up-4">

      <?php while ( $services->have_posts() ) : $services->the_post(); ?>

        <div class="column service-icons__column">
          <a class="service-icons__link" href="<?php the_permalink(); ?>">
            <div class="service-icons__box text-center">
              <div class="service-icons__icon service-icons__icon--<?php echo sanitize_title( get_the_title() ); ?>"></div>
              <?php
              the_title( '<h4 class="service-icons__icon__title">', '</h4>' );
              if ( $hide_excerpts != true ) {
                echo '<p class="service-icons__icon__excerpt">' . esc_html__( get_field('icon_grid_excerpt'), 'ssi' ) . '</p>';
              }
              ?>
            </div>
          </a>
        </div>

      <?php endwhile; ?>

    </div>
  </section>

<?php endif; wp_reset_postdata(); ?>
