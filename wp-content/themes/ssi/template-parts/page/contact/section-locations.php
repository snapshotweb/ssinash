<?php
$args = array(
  'post_type'       => 'location',
  'posts_per_page'  => -1
);

$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) :
  $i = 0;
  ?>

  <section class="contact-page__locations">

    <div class="row expanded collapse" data-equalizer data-equalize-on="medium">

      <div class="medium-4 columns">
        <ul class="vertical tabs contact-page__locations__tabs" id="location-tabs" data-equalizer-watch>

          <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>

            <li class="contact-page__locations__tabs__item tabs-title<?php if ( $i == 1 ) { echo ' is-active'; } ?>"
                style="background-image:url('<?php echo get_template_directory_uri() . '/assets/img/img_skyline_' . sanitize_title( get_the_title() ) . '.jpg'; ?>');">
                <?php $location = get_field('address'); ?>
              <div id="map-button-<?php echo sanitize_title( get_the_title() ); ?>" <?php if ( $i == 1 ) { echo ' aria-selected="true"'; } ?>
                 class="contact-page__locations__tabs__link" data-lat="<?php echo esc_attr( $location['lat'] ); ?>" data-lng="<?php echo esc_attr( $location['lng'] ); ?>" data-location-name="<?php the_title(); ?>">

                  <address itemscope itemtype="http://schema.org/LocalBusiness">
                    <h4 itemprop="name"><?php the_title(); ?> <?php esc_html_e( 'Office', 'ssi' ); ?></h4>
                    <span itemprop="telephone"><?php echo esc_html( get_field('phone') ); ?></span><br />
                    <span itemprop="streetAddress"><?php ssi_get_location_info('address');
                      if ( get_field('suite_number') ) {
                        echo ', Suite ' . ssi_get_location_info( 'suite_number', false );
                      } ?></span><br />
                    <span itemprop="addressLocality"><?php ssi_get_location_info('city'); ?></span>,
                    <span itemprop="addressRegion"><?php ssi_get_location_info('state'); ?></span>
                    <span itemprop="postalCode"><?php ssi_get_location_info('zip'); ?></span>
                  </address>

              </div>
            </li>

          <?php endwhile; $i = 0; ?>

        </ul>
      </div><!-- .columns -->

      <div class="medium-8 columns">
        <div class="contact-page__locations__tabs__content tabs-content" data-equalizer-watch>

            <div class="contact-page__locations__tabs__panel">
              <div id="map-container" class="contact-page__map"></div>
            </div>

        </div>
      </div><!-- .columns -->

    </div><!-- .row -->

  </section>

<?php endif; ?>
