<?php if ( have_rows('block_links') ) : ?>

  <section class="contact-page__block-links section--padded--extra">
    <div class="row">

      <?php while ( have_rows('block_links') ) : the_row(); ?>

        <div class="columns medium-6">
          <a href="<?php echo esc_url( get_sub_field('link') ) ?>" class="contact-page__block-links__block__link" style="background-image:url('<?php echo esc_url( get_sub_field('background_image')['sizes']['large'] ); ?>');">
            <div class="contact-page__block-links__block">
              <span class="contact-page__block-links__block__content"><?php esc_html_e( get_sub_field('label'), 'ssi' ) ?></span>
            </div>
          </a>
        </div>

      <?php endwhile; ?>

    </div>
  </section>

<?php endif; ?>
