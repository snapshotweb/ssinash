<section class="contact-page__form-section section--padded--extra">
  <div class="row">
    <div class="columns medium-6 large-5">
      <?php the_content(); ?>
    </div><!-- .columns -->
    <div class="columns medium-6 large-7">
      <div class="contact-page__form">
        <?php echo do_shortcode( get_field('form_shortcode') ); ?>
      </div>
    </div><!-- .columns -->
  </div><!-- .row -->
</section>
