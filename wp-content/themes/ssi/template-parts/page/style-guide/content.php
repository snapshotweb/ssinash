<!-- Begin top navigation -->
<?php if ( have_rows('company') && count( get_field('company') ) > 1 ) : ?>

  <nav class="style-guide__top-nav section--padded--less">
    <div class="row">
      <div class="small-12 columns">
        <ul class="tabs style-guide__top-nav__list" data-tabs id="nav-tabs">

          <?php $i = 0; while ( have_rows('company') ) : the_row(); $i++; ?>

            <li class="tabs-title<?php echo $i == 1 ? ' is-active' : null; ?> style-guide__top-nav__list__item">
              <a class="style-guide__top-nav__list__link" href="#panel<?php echo $i; ?>"<?php echo $i == 1 ? ' aria-selected="true"' : null; ?>>
                <?php echo esc_html( get_sub_field('company_name') ); ?>
              </a>
            </li>

          <?php endwhile; ?>

        </ul>
      </div>
    </div>
  </nav>

<?php endif; ?>


<?php if ( have_rows('company') ) : ?>

  <section class="style-guide__content-section section--padded">
    <div class="row tabs-content" data-tabs-content="nav-tabs">

      <?php get_template_part( 'template-parts/page/style-guide/sections' ); ?>

    </div><!-- .row -->
  </section>

<?php endif; ?>
