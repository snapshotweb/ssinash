<?php $i = 0; while ( have_rows('company') ) : the_row(); $i++; ?>

  <?php
  // Construct tabs-panel classes
  $panel_class  = 'tabs-panel';
  $panel_class .= $i == 1 ? ' is-active' : null;
  $panel_class .= ' style-guide__content-section--' . sanitize_title( get_sub_field('company_name') );
  $panel_class .= ' columns small-12';
  ?>

  <div class="<?php echo $panel_class; ?>" id="panel<?php echo $i; ?>">
    <div class="row">

      <?php if ( have_rows('sections') ) : ?>

        <div class="columns medium-4 large-3">
          <nav class="style-guide__side-nav">
            <ul class="tabs vertical tabs style-guide__side-nav__list" data-tabs id="side-nav-tabs<?php echo $i; ?>">

              <?php $c = 0; while ( have_rows('sections') ) : the_row(); $c++; ?>

                <?php
                // Add leading zero to count
                $count = $c < 10 ? '0' . $c : $c;
                // Set variable to count panels
                // Use combination of tab count and company count to ensure no repeat values
                $p = $i . $c;
                ?>

                <li class="tabs-title<?php echo $c == 1 ? ' is-active' : null; ?> style-guide__side-nav__list__item">
                  <a class="style-guide__side-nav__list__link" href="#panel<?php echo $p; ?>"<?php echo $i == 1 ? ' aria-selected="true"' : null; ?>>
                    <?php printf( esc_html__( '%s. ' . get_sub_field('title'), 'ssi' ), $count ); ?>
                  </a>
                </li>

              <?php endwhile; ?>

            </ul>
          </nav>
        </div><!-- .columns -->

      <?php endif; ?>

      <?php if ( have_rows('sections') ) : ?>

        <div class="columns medium-8 large-9 tabs-content" data-tabs-content="side-nav-tabs<?php echo $i; ?>">

          <?php $c = 0; while ( have_rows('sections') ) : the_row(); $c++; ?>

            <?php
            // Add leading zero to count
            $count = $c < 10 ? '0' . $c : $c;
            // Set variable to count panels
            // Use combination of tab count and company count to ensure no repeat values
            $p = $i . $c;
            ?>

            <section class="tabs-panel<?php echo $c == 1 ? ' is-active' : null; ?> style-guide__content" id="panel<?php echo $p; ?>">
              <h2 class="style-guide__content__title">
                <?php printf( esc_html__( '%s. ' . get_sub_field('title'), 'ssi' ), $count ); ?>
              </h2>
              <div class="style-guide__content__subtitle">
                <?php the_sub_field('subtitle'); ?>
              </div>
              <?php
              if ( have_rows('content') ) {
                get_template_part( 'template-parts/page/style-guide/sections/content' );
              }
              ?>
            </section>

          <?php endwhile; ?>

        </div><!-- .tabs-content -->

      <?php endif; ?>

    </div><!-- .row -->
  </div><!-- .columns -->

<?php endwhile; ?>
