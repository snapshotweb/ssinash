<?php $layout = str_replace( '_', '-', get_row_layout() ); ?>

<div class="row">
  <div class="large-4 columns">
    <?php the_sub_field('left_column'); ?>
  </div>
  <div class="large-8 columns">
    <?php the_sub_field('right_column'); ?>
  </div>
</div>
