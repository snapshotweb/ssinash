<?php
$layout = str_replace( '_', '-', get_row_layout() );
$count = count( get_sub_field('colors') );
$row_class = 'row';
if ( $count < 3 ) {
  $row_class .= ' small-up-2';
}
elseif ( $count === 3 ) {
  $row_class .= ' small-up-2 medium-up-3';
}
else {
  $row_class .= ' small-up-2 medium-up-4';
}
?>

<div class="row">
  <div class="large-4 columns">
    <?php the_sub_field('content'); ?>
  </div>
  <div class="large-8 columns">
    <div class="<?php echo $row_class; ?>">

      <?php while ( have_rows('colors') ) : the_row(); ?>

        <div class="column">
          <div class="style-guide__content__color style-guide__content__color--<?php echo sanitize_title( get_sub_field('color_name') ); ?>"
               style="background-color:<?php the_sub_field('hex'); ?>;">
            <div class="style-guide__content__color__content">
              <h4 class="style-guide__content__color__name">
                <?php esc_html_e( get_sub_field('color_name'), 'ssi' ); ?>
              </h4>
            </div>
          </div>
          <!-- .style-guide__content__color -->
          <ul class="style-guide__content__color__list">
            <li class="style-guide__content__color__list__item">
              <?php echo esc_html( 'Hex: ' . get_sub_field('hex') ); ?>
            </li>
            <li class="style-guide__content__color__list__item">
              <?php echo esc_html( 'RGB: ' . get_sub_field('rgb') ); ?>
            </li>
            <li class="style-guide__content__color__list__item">
              <?php echo esc_html( 'CMYK: ' . get_sub_field('cmyk') ); ?>
            </li>
            <li class="style-guide__content__color__list__item">
              <?php echo esc_html( 'Pantone: ' . get_sub_field('pantone') ); ?>
            </li>
          </ul>
        </div><!-- .column -->

      <?php endwhile; ?>

    </div><!-- .row -->
  </div><!-- .columns -->
</div><!-- .row -->
