<?php while ( have_rows('content') ) : the_row(); ?>

  <?php
  $layout        = get_row_layout();
  // Construct container classes
  $layout_class  = 'style-guide__content__block';
  $layout_class .= ' ' . $layout_class . '--' . str_replace( '_', '-', $layout );
  ?>

  <div class="<?php echo $layout_class; ?>">

    <?php get_template_part( 'template-parts/page/style-guide/sections/block', esc_attr($layout) ); ?>

  </div>

<?php endwhile; ?>
