<?php $layout = str_replace( '_', '-', get_row_layout() ); ?>

<div class="row">
  <div class="large-4 columns">
    <?php the_sub_field('content'); ?>
  </div>
  <div class="large-8 columns">
    <div class="row small-up-2 medium-up-3 large-up-4">
      <?php while ( have_rows('icons') ) : the_row(); ?>
        <div class="column">
          <?php
          $icon  = get_sub_field('icon');
          $label = get_sub_field('label');
          ?>
          <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" class="style-guide__content__icon" />
          <?php if ( $label ) : ?>
            <p class="style-guide__content__icon__label">
              <?php esc_html_e( $label, 'ssi' ); ?>
            </p>
          <?php endif; ?>
        </div>
      <?php endwhile; ?>
  </div>
</div>
