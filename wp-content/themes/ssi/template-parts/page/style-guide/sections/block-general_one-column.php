<?php $layout = str_replace( '_', '-', get_row_layout() ); ?>

<div class="row">
  <div class="small-12 columns">
    <?php the_sub_field('content'); ?>
  </div>
</div>
