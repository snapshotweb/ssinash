<section class="blog__categories-list section--padded--less show-for-medium">
  <div class="row">
    <div class="columns small-12">
      <ul>
        <?php wp_list_categories( array(
          'show_option_all'   => 'All',
          'orderby'           => 'name',
          // Animation, Behind the Scenes, Video Production, Web Development
          'include'           => array( 49, 50, 52, 51 ),
          'title_li'          => '',
          'hide_empty'        => false
        ) ); ?>
      </ul>
    </div>
  </div>
</section>
