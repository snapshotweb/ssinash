<section class="careers section--padded--extra">
  <div class="row">
    <div class="columns small-12">
      <h2 class="careers__heading"><?php _e( 'Current Openings', 'ssi' ); ?></h2>
    </div>
  </div>
  <?php
  $args = array(
    'post_type'       => 'career',
    'posts_per_page'  => -1
  );

  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) :
    while ( $the_query->have_posts() ) :
      $the_query->the_post();
      $location = get_field('location');
      $specs = get_field('spec_sheet');
      ?>

      <div class="row careers__single-career__row">
        <div class="columns small-12 careers__single-career__column">
          <div class="careers__single-career">
            <header class="careers__single-career__header">
              <span class="careers__single-career__location">
                <?php echo get_the_title( $location->ID ); ?>
              </span>
              <h3 class="careers__single-career__label"><?php the_title(); ?></h3>
              <a class="button careers__single-career__button careers__single-career__button--apply" data-open="contact-modal" data-value="<?php the_title(); ?>">
                <?php _e( 'Apply for Position', 'ssi' ); ?>
              </a>
            </header>
            <div class="careers__single-career__content">
              <?php the_content(); ?>
              <?php if ( $specs ) : ?>
                <a href="<?php echo $specs['url']; ?>" target="_blank" class="careers__single-career__read-more-link read-more-link">
                  <?php _e( 'Read More', 'ssi' ); ?>
                </a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

    <?php endwhile; ?>

    <div class="reveal" id="contact-modal" data-reveal>
      <div class="row">
        <div class="columns small-12">
          <h2 class="reveal__heading heading-underline"><?php printf( __( 'Work with %s', 'ssi' ), 'SnapShot' ); ?></h2>
          <?php echo do_shortcode('[contact-form-7 id="326" title="Job Application"]'); ?>
        </div>
      </div>
      <button class="close-button reveal__close-button" data-close aria-label="Close modal" type="button">
        <?php echo _x( 'Close', 'Click here to close the modal', 'ssi' ); ?> <span class="reveal__close-button__x" aria-hidden="true">&#xe870;</span>
      </button>
    </div>

  <?php else : ?>

    <div class="row">
      <div class="columns small-12">
        <p><?php _e( 'Sorry, there are no openings currently, but we are always willing to talk about opportunities with the right people!', 'ssi' ); ?></p>
        <p><a class="button" href="/contact-us"><?php _e( 'Get In Touch', 'ssi' ); ?></a></p>
      </div>
    </div>

  <?php endif; wp_reset_postdata(); ?>
</section>
