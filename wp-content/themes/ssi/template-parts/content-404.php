<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */
?>

<section class="error-404 not-found">
  <div class="row">
    <div class="columns small-12">
      <header class="page-header">
        <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'ssi' ); ?></h1>
      </header><!-- .page-header -->

      <div class="page-content">
        <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ssi' ); ?></p>

      </div><!-- .page-content -->
    </div><!-- .columns -->
  </div><!-- .row -->
</section><!-- .error-404 -->
