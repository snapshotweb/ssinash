<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

if ( post_password_required() ) :
  ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
      <div class="columns medium-8 large-6 medium-centered text-center section--padded--extra">
        <?php echo get_the_password_form(); ?>
      </div>
    </div>
  </article>

<?php
else :

  if ( get_the_content() != null ) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <div class="row">
        <div class="columns small-12">
          <div class="entry-content">
            <?php the_content(); ?>
          </div><!-- .entry-content -->

          <?php if ( get_edit_post_link() ) : ?>
            <footer class="entry-footer">
              <?php
              edit_post_link(
                sprintf(
                /* translators: %s: Name of current post */
                  esc_html__( 'Edit %s', 'ssi' ),
                  the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ),
                '<span class="edit-link">',
                '</span>'
              );
              ?>
            </footer><!-- .entry-footer -->
          <?php endif; ?>
        </div>
      </div>
    </article><!-- #post-## -->

    <?php
  endif;
  get_template_part( 'template-parts/section', 'content-blocks' );
endif;
