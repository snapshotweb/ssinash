<div class="site__nav__overlay">
  <div class="row">
    <div class="columns medium-4 large-3 xlarge-2">
      <?php
      wp_nav_menu( array(
        'theme_location'  => 'main',
        'container_class' => 'site__nav__menu-container site__nav__main-menu-container',
        'menu_class'      => 'site__nav__menu site__nav__main-menu menu'
      ));
      wp_nav_menu( array(
        'theme_location'  => 'secondary',
        'container_class' => 'site__nav__menu-container site__nav__secondary-menu-container',
        'menu_class'      => 'site__nav__menu site__nav__secondary-menu menu'
      ));
      ?>
      <div class="site__nav__newsletter" id="menu-newsletter">
        <h4>Stay up to date</h4>
        <?php echo do_shortcode('[contact-form-7 id="87" title="Emma Newsletter Signup" html_class="site__nav__newsletter__form"]'); ?>
        <p class="site__nav__newsletter__response"></p>
      </div><!-- .site__nav__newsletter -->
    </div><!-- .columns -->
    <div class="columns medium-8 large-9 xlarge-10">
      <div class="site__nav__contact-block">
        <p class="site__nav__contact-block__phone">
          <?php
          $phone = get_field( 'phone', 'option' );
          $email = get_field( 'email', 'option' );
          ?>
          Phone:
          <a href="tel:<?php echo $phone; ?>">
            <?php echo $phone; ?>
          </a>
        </p>
        <p class="site__nav__contact-block__email">
          Email:
          <a href="mailto:<?php echo $email; ?>">
            <?php echo $email; ?>
          </a>
        </p>
      </div><!-- .site__nav__contact-block -->
      <div class="site__nav__location-block">

        <?php
        $loop = new WP_Query( array(
          'post_type' => 'location',
          'posts_per_page' => -1
        ));
        while ( $loop->have_posts() ) : $loop->the_post();

          // Vars
          $location = get_field('address');
          $location = explode( ',', $location['address'] );
          $address  = $location[0];
          $suite    = get_field('suite_number');
          $city     = $location[1];
          $state    = $location[2];
          $zip      = get_field('zip');
          ?>

          <address>
            <?php the_title( '<h4 itemprop="name">', '</h4>' ); ?>
            <?php the_field('phone'); ?><br />
            <span itemprop="streetAddress"><?php echo $address; ?>
            <?php if ( $suite ) {
              echo ', Suite ' . $suite;
            } ?></span><br />
            <span itemprop="addressLocality"><?php echo $city; ?></span>,
            <span itemprop="addressRegion"><?php echo $state; ?></span>
            <span itemprop="postalCode"><?php echo $zip; ?></span>
          </address>

        <?php endwhile; wp_reset_query(); ?>

      </div><!-- .site__nav__location-block -->
    </div><!-- .columns -->
  </div><!-- .row -->
</div><!-- .site__nav__overlay -->