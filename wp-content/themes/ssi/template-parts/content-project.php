<?php
/**
 * Template part for displaying project content
 *
 * @package SnapShot_Interactive
 */

?>
<section class="single-project__content-area section--padded">
  <div class="row">
    <div class="columns medium-8 large-9">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if ( get_field('hide_entry_title') != true ) : ?>
          <header class="entry-header">
            <?php the_title( '<h1 class="entry-title single-project__title">', '</h1>' ); ?>
          </header>
        <?php endif; ?>
        <?php the_content(); ?>
      </article>
    </div>
    <div class="columns medium-4 large-3">
      <?php get_sidebar( 'project' ); ?>
    </div>
  </div>
</section>
