<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

global $blog_post_count;
if ( get_the_post_thumbnail() ) {
  $thumb = get_the_post_thumbnail_url();
} else {
  $thumb = get_template_directory_uri() . '/assets/img/placeholder_blog.jpg';
}
// Set variables for article attributes
$post_open .= '<div class="column blog__post--featured__column">';
$post_close = '</div>';
$post_class .= ' blog__post--featured';
$thumb_class .= ' blog__post--featured__thumb';
$info_class .= ' blog__post--featured__info';
$title_class .= ' blog__post--featured__title';
$meta_class .= ' blog__post--featured__posted-on';
$excerpt = '<div class="entry-summary">' . get_the_excerpt() . '</div>';
?>

<?php echo $post_open; ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
    <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
      <div class="<?php echo $thumb_class; ?>" style="background-image: url('<?php echo esc_url($thumb); ?>');"></div>
    </a>
    <div class="<?php echo $info_class; ?>"<?php echo $info_data; ?>>
      <header class="entry-header">
        <?php the_title( sprintf( '<h3 class="%1$s"><a href="%2$s" rel="bookmark">', $title_class, esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        <p class="<?php echo $meta_class; ?>"><?php ssi_posted_on(); ?></p>
      </header>
      <?php echo $excerpt; ?>
    </div>
  </article>

<?php echo $post_close; ?>
