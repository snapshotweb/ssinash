<?php
$video = get_field('vimeo_id');
$header_style = get_field('header_style');
?>

<section class="page-header page-header--project<?php if ( $video && ( $header_style != null || $header_style != 'none' ) ) : ?> page-header--project--no-padding<?php endif; ?>">
  <div<?php if ( $video ) : ?> class="section--black"<?php endif; ?>>
    <div class="row">

      <?php if ( $video ) : ?>

        <div class="columns medium-10 medium-centered">
          <div class="page-header--project__video">
            <div class="vimeo-embed-container">
              <iframe src="//player.vimeo.com/video/<?php echo esc_attr($video); ?>?title=0&byline=0&portrait=0&autoplay=1"
                      frameborder="0"
                      webkitAllowFullScreen
                      mozallowfullscreen
                      allowFullScreen></iframe>
            </div>
          </div>

        <?php else : ?>

          <div class="columns small-12">
            <div class="page-header--project__thumb"
                 style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
            </div>
          </div>

        <?php endif; ?>

      </div>
    </div>
  </div>
</section>
