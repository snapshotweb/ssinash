<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="landing-page__header__flag">
  <?php get_template_part( 'template-parts/icon', 'ssi' ); ?>
</a>
<?php
if ( get_field('header_background_image') ) {
  $header_background_image = get_field('header_background_image');
}
?>
<header class="landing-page__header" style="background-image: url(<?php echo esc_url( $header_background_image['url'] ); ?>);">
  <div class="row">
    <div class="small-12 large-8 columns title">
      <?php
      if ( get_field('page_title') ) {
        $page_title = get_field('page_title');
      } else {
        $page_title = get_the_title();
      }
      // Get last word of string by making it an array, then popping off the last word
      $title_array     = explode( ' ', $page_title );
      $last_word       = array_pop($title_array);
      $title_beginning = implode( ' ', $title_array );

      echo '<h1 class="landing-page__title">' . esc_html__( $title_beginning, 'ssi' ) . ' ';
      echo '<span class="landing-page__title__last-word">' . esc_html__( $last_word, 'ssi' ) . '</span></h1>';
      ?>
    </div><!-- .columns -->
    <div class="small-12 medium-6 large-4 columns landing-page__form">
      <h4 class="landing-page__form__title"><?php esc_html_e( get_field('contact_form_title'), 'ssi' ); ?></h4>
      <?php
      $form_shortcode = sanitize_text_field( get_field('contact_form_shortcode') );
      echo do_shortcode( $form_shortcode );
      ?>
      <?php if ( get_field('file_download') ) : ?>
        <a class="landing-page__download-link" href="<?php echo esc_url( get_field('download_item')['url'] ); ?>" target="_blank">
          <?php _e( 'Click Here to Download', 'ssi' ); ?>
        </a>
      <?php endif; ?>
    </div><!-- .columns -->
  </div> <!-- .row  -->
</div><!-- .landing-page__header -->
