<?php $header_style = get_field('header_style'); ?>

<header id="masthead"
        class="site__header<?php if ( $header_style == 'hero' ) { echo ' site__header--transparent'; } ?>"
        role="banner">
  <div class="row">
    <div class="columns small-6 medium-4 large-3">
      <?php
      if ( is_front_page() ) :
        get_template_part( 'template-parts/icon', 'ssi' );
      else : ?>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
          <?php get_template_part( 'template-parts/icon', 'ssi' ); ?>
        </a>
      <?php endif; ?>
    </div><!-- .columns -->
    <div class="columns small-6 medium-8 large-9">

      <?php if ( get_field('add_cta_in_header') == true ) : ?>
        <a class="site__header__cta button hollow" href="<?php the_field('cta_link'); ?>"><?php the_field('cta_text'); ?></a>
      <?php endif; ?>

      <?php
      if ( get_field('show_menu_in_header') == true ) {
        wp_nav_menu( array(
          'menu_id' => get_field('menu')
        ));
      } ?>

      <?php if ( get_field('hide_hamburger_menu') != true ) : ?>
        <?php get_template_part( 'template-parts/main-site-nav/nav', 'hamburger' ); ?>
        <ul class="search-toggle">
          <li class="search-toggle__item search-toggle__item--button" id="search-click">
            <a href="javascript:void(0)">
              <i class="icon-search" aria-hidden="true"></i>
            </a>
          </li>
          <li class="search-toggle__item search-toggle__item--field" id="top-search">
            <form action="<?php echo get_site_url('/'); ?>" method="post">
              <input id="top-search-string" name="s" placeholder="Search...">
            </form>
          </li>
        </ul>
      <?php endif; ?>


    </div><!-- .columns -->
  </div><!-- .row -->
</header><!-- #masthead -->

<?php get_template_part( 'template-parts/main-site-nav/overlay' ); ?>
