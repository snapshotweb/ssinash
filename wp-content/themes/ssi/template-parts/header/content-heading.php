<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>

<header>
  <h1 class="section-heading--stylized"><?php
  if ( $heading ) :

    $allowed_tags = array(
      'br'  => array(),
      'p'  => array(),
    );

    _e( wp_kses( $heading, $allowed_tags ), 'ssi' );

    if ( $heading_emphasis ) :
      echo '<strong>' . esc_html__( $heading_emphasis, 'ssi' ) . '</strong>';
    endif;

  elseif ( is_archive() ) :

    the_archive_title();

  else :

    the_title();

  endif;
  ?></h1>
  <?php if ( is_singular( 'post' ) ) : ?>
    <p class="page-header__posted-on"><?php ssi_posted_on(); ?></p>
  <?php endif; ?>
</header>
