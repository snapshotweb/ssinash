<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>
<div class="page-header--hero__content">
  <div class="row">
    <div class="column small-10 medium-8 large-5 small-centered">

      <?php
      get_template_part( 'template-parts/header/content', 'heading' );
      echo $header_content;
      ?>

    <?php if ( $show_video_link == true && ! post_password_required() ) : ?>

      <div class="play-button page-header__play-button__link text-center">
        <a href="//vimeo.com/<?php echo esc_attr($vimeo_id); ?>" data-lity>
          <i class="page-header__play-button icon icon--play" aria-hidden="true"></i>
        </a>
      </div><!-- .play-button -->

    <?php endif; ?>

    </div><!-- .columns -->
  </div><!-- .row -->
</div><!-- .page-header--hero__content -->
