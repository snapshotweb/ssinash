<?php
// Variables
if ( get_field('hero_image') ) {
  $image = get_field('hero_image');
  $image = $image['url'];
} else {
  $image = get_the_post_thumbnail_url();
} ?>

<header class="page-header page-header--team page-header--hero page-header--hero--bg__image"
         style="background-image: url('<?php echo esc_url($image); ?>');">
  <div class="row">
    <div class="columns medium-8 large-6">
    </div>
    <div class="columns medium-8 large-6">
      <div class="page-header--team__content">
        <?php the_title( '<h1 class="page-header--team__name">', '</h1>' ); ?>
        <div class="page-header--team__bio">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>

</header>
