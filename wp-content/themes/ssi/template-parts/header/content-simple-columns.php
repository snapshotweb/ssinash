<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>
<div class="columns medium-4">
  <?php get_template_part( 'template-parts/header/content', 'heading' ); ?>
</div>
<div class="columns medium-8">
  <?php echo $header_content; ?>
</div>
