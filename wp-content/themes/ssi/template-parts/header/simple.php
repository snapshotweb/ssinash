<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>

<section class="page-header page-header--simple page-header--simple--<?php echo $text_orientation; ?>">
  <div class="row">
    <?php get_template_part( 'template-parts/header/content-simple', $text_orientation ); ?>
  </div><!-- .row -->
</section>
