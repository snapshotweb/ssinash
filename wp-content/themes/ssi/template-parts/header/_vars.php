<?php
if ( is_archive() ) {
  $archive_slug = get_queried_object()->name;
  $archive_group_field = get_field( 'archive_' . $archive_slug, 'option' );
  $archive_header_style = $archive_group_field['header_style'];
}

// Array of all field slugs in `Header Options` field group
$header_fields = array(
  'header_style',
  'text_orientation',
  'background_style',
  'background_image',
  'poster_image',
  'header_content',
  'heading',
  'heading_emphasis',
  'show_video_link',
  'vimeo_id',
  'show_overlay',
);

// Define variables for each field
foreach ( $header_fields as $field_name ) {
  ${$field_name} = $archive_header_style ? $archive_group_field[$field_name] : get_field($field_name);
}

// Set header style to null if nothing is set for archive pages
if ( is_archive() && ! $archive_header_style ) {
  $header_style = null;
}

if ( $background_style == 'image' ) {
  if ( $background_image ) {
    $image = esc_url( $background_image['url'] );
  } elseif ( get_the_post_thumbnail() ) {
    $image = get_the_post_thumbnail_url();
  }
} elseif ( $background_style == 'video' ) {
  $image = esc_url( $poster_image['url'] );
}
