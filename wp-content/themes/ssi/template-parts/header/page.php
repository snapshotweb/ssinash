<?php
include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables

if ( is_singular( 'project' ) && get_field('hide_featured') != true ) {

  get_template_part( 'template-parts/header/project' );

} elseif ( is_singular( 'team' ) ) {

  get_template_part( 'template-parts/header/team' );

} elseif ( is_page_template( 'page-templates/page-contact.php' ) ) {

  get_template_part( 'template-parts/page/contact/header' );

} elseif ( is_page_template( 'page-templates/page-landing-page.php' ) ) {

  get_template_part( 'template-parts/header/landing' );

} elseif ( ! empty($header_style) ) {

  get_template_part( 'template-parts/header/' . $header_style );

} else {
  // Do nothing.
}
