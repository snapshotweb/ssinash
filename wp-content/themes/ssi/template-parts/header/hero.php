<?php include get_template_directory() . '/template-parts/header/_vars.php'; // Include header variables ?>

<section class="page-header page-header--hero page-header--hero--bg__<?php echo $background_style; ?>"
         style="background-image: url('<?php echo $image; ?>');">

<?php if ( $background_style == 'video' ) : ?>

  <video class="page-header__bg--video" poster="<?php echo $image; ?>" id="bgvid" playsinline autoplay muted loop>

    <?php
    if ( have_rows('background_video_source') ) :
      while ( have_rows('background_video_source') ) :
        the_row();
        // Instantiate class to detect mobile devices
        $detect = new Mobile_Detect;
        if ( ! $detect->isMobile() ) :
          ?>

          <source src="<?php echo esc_url( get_sub_field('source') ); ?>"
                  type="video/<?php echo esc_attr( get_sub_field('filetype') ); ?>">
                  
          <?php
        endif;
      endwhile;
    endif;
    ?>

  </video>

<?php endif; ?>

<?php if ( $show_overlay == true ) : ?>

  <div class="page-header__overlay"></div>

<?php endif; ?>

  <?php get_template_part( 'template-parts/header/content', 'hero' ); ?>

</section>
