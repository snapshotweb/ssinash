<?php
$section_title = get_field('bio_section_title');
$team_bio = get_field('team_bio');
$num_of_bios = count( $team_bio );
if ( $team_bio ) :
  if ( $num_of_bios > 1 ) {
    $section_atts = 'class="team-bio-section team-bio-section--slider" data-equalizer data-equalize-on="small"';
  } else {
    $section_atts = 'class="team-bio-section"';
  }
  ?>

  <section <?php echo $section_atts; ?>>

    <?php
    foreach ( $team_bio as $post ) :
      setup_postdata( $post );
      // Variables
      if ( get_field('banner_image') ) {
        $image = get_field('banner_image');
        $image = $image['url'];
      } elseif ( get_field('hero_image') ) {
        $image = get_field('hero_image');
        $image = $image['url'];
      } else {
        $image = get_the_post_thumbnail_url();
      }
      $equalizer = ( $num_of_bios > 1 ) ? ' data-equalizer-watch' : null;
      ?>

      <div class="team-bio-section__background-image section--padded--extra" style="background-image: url(<?php echo esc_url($image); ?>);"<?php echo $equalizer; ?>>
        <div class="row">
          <div class="columns medium-8 large-6">
          </div>
          <div class="columns medium-8 large-6">
            <div class="team-bio-section__content">
              <?php
              if ( $section_title ) {
                printf( esc_html__( '%1$s' . $section_title . '%2$s', 'ssi'  ), '<h2 class="team-bio-section__title">', '</h2>' );
              }
              ?>
              <?php the_title( '<h3 class="team-bio-section__name">', '</h3>' ); ?>
              <div class="team-bio-section__bio">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
        </div>

      </div>

    <?php endforeach; ?>

  </section>

<?php
endif;
wp_reset_postdata();
