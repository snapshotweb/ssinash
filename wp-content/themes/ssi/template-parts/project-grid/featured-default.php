<?php
// Retrieve project type using Yoast SEO Primary Term
$primary_term = new WPSEO_Primary_Term( 'project-type', $featured_project->ID );
$primary_term = get_term( $primary_term->get_primary_term() );
?>

<div class="row collapse">

  <div class="columns medium-7 large-8 medium-push-5 large-push-4">
    <div class="featured-project__thumb"
         style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url() ); ?>);"
         data-equalizer-watch>

      <?php
      // Show play icon if Vimeo link is active
      if ( get_field('vimeo_id') ) {
        get_template_part( 'template-parts/icon', 'play' );
      }
      ?>

    </div><!-- .featured-project__thumb -->
  </div><!-- .columns -->

  <?php
  if ( get_field('alternate_image') ) {
    $alt_image = get_field('alternate_image');
    $alt_image = $alt_image['url'];
  } else {
    $alt_image = get_the_post_thumbnail_url();
  }
  ?>

  <div class="columns medium-5 large-4 medium-pull-7 large-pull-8">

    <div class="featured-project__description"
         style="background-image: url(<?php echo esc_url($alt_image); ?>);"
         data-equalizer-watch>

      <header class="featured-project__description__heading">
        <?php the_title( '<h3 class="featured-project__description__title">', '</h3>' ); ?>
        <h4 class="featured-project__description__subtitle"><?php esc_html_e( 'Featured ' . $primary_term->name . ' Project', 'ssi' ); ?></h4>
      </header>
      <?php the_content(); ?>

    </div><!-- .featured-project__description -->
  </div><!-- .columns -->
</div><!-- .row -->
