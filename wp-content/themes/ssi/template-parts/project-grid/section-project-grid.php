<section class="project-grid">

  <?php
  if ( get_field('featured_project') ) {
    get_template_part( 'template-parts/project-grid/section', 'featured' );
  }
  get_template_part( 'template-parts/project-grid/list', 'projects' );
  ?>

</section>
