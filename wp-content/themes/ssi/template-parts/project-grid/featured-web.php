<?php
// Retrieve project type using Yoast SEO Primary Term
$primary_term = new WPSEO_Primary_Term( 'project-type', $featured_project->ID );
$primary_term = get_term( $primary_term->get_primary_term() );

if ( get_field('alternate_image') ) {
  $alt_image = get_field('alternate_image');
  $alt_image = $alt_image['url'];
} else {
  $alt_image = get_the_post_thumbnail_url();
}
?>

<div class="row collapse">

  <div class="columns small-12">
    <div class="featured-project__thumb featured-project__thumb--web"
         style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url() ); ?>);">

      <div class="featured-project__description featured-project__description--web"
           style="background-image: url(<?php echo esc_url($alt_image); ?>);">
        <header class="featured-project__description__heading">
          <?php the_title( '<h3 class="featured-project__description__title">', '</h3>' ); ?>
          <h4 class="featured-project__description__subtitle"><?php esc_html_e( 'Featured ' . $primary_term->name . ' Project', 'ssi' ); ?></h4>
        </header>
        <?php the_content(); ?>
      </div><!-- .featured-project__description -->

    </div><!-- .featured-project__thumb -->
  </div><!-- .columns -->
</div><!-- .row -->
