<section class="project-nav">

  <div class="row">
    <div class="small-12 columns">
      <div class="project-nav__project-type filters-select section--padded">

        <?php
        $args = array(
          'taxonomy'      => 'project-type',
          'parent'        => 0,
          'hide_empty'    => true
        );
        $terms = get_terms( $args );
        ?>

        <button class="project-nav__button is-checked" data-filter="*">View All</button>

        <?php foreach ( $terms as $term ) : ?>
          <button class="project-nav__button" data-filter="<?php echo '.' . $term->slug; ?>"><?php echo $term->name; ?></button>
        <?php endforeach; ?>

      </div>
    </div><!-- .columns -->
  </div><!-- .row -->

</section>
