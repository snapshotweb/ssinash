<section class="featured-project" data-equalizer data-equalize-on="medium">

  <?php
  $featured_project = get_field('featured_project');
  $project_type = get_field('project_type');
  if ( $featured_project ) :
    $post = $featured_project;
    setup_postdata( $post );

    // Set hyperlink to Vimeo embed if applicable, post link if not
    if ( get_field('vimeo_id') ) {
      $featured_atts = 'href="//vimeo.com/' . esc_attr( get_field('vimeo_id') ) . '" data-lity';
    } else {
      $featured_atts = 'href="' . esc_url( get_the_permalink() ) . '"';
    }
    ?>

    <style>
    .featured-project__description {
      background-blend-mode: overlay, screen, multiply;
    }
    <?php
    $featured_color = get_field('alternate_background');
    if ( $featured_color ) : ?>

      a:hover .featured-project__description,
      a:focus .featured-project__description,
      a:hover .featured-project__description::after,
      a:focus .featured-project__description::after,
      .play-icon:hover,
      .play-icon:focus,
      a:hover .play-icon,
      a:focus .play-icon {
        background-color: <?php echo esc_attr($featured_color); ?>;
      }
    <?php endif; ?>

    </style>

    <div class="row">
      <div class="columns small-12">

        <a <?php echo $featured_atts; ?>>
          <div class="featured-project__container">

            <?php
            // If project type is `development`
            if ( $project_type == 45 ) {
              get_template_part( 'template-parts/project-grid/featured', 'web' );
            } else {
              get_template_part( 'template-parts/project-grid/featured', 'default' );
            }
            ?>

          </div><!-- .featured-project__container -->
        </a>

      </div><!-- .columns -->
    </div><!-- .row -->

  <?php endif; wp_reset_postdata(); ?>

</section>
