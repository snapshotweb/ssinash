<?php
$project_type = get_field('project_type');
$row_classes = 'row iso-grid medium-up-2';
// If project type is NOT `development`
if ( ! $project_type == 45 ) {
  $row_classes .= ' large-up-3';
}
?>
<section class="project-list">
  <div class="<?php echo $row_classes; ?>">

    <?php
    $args = array(
      'post_type'       => 'project',
      'posts_per_page'  => -1
    );

    if ( $project_type ) {
      $args['tax_query'] = array(
        array(
          'taxonomy'    => 'project-type',
          'field'       => 'term_id',
          'terms'       => $project_type
        )
      );
    }

    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) :
    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

      <?php get_template_part( 'template-parts/project-grid/post', 'project' ); ?>

    <?php endwhile; endif; wp_reset_postdata(); ?>

  </div><!-- .row -->
</section>
