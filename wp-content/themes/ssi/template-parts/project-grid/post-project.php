<?php
// Retrieve tax terms to apply class names for Isotope sorting
$taxonomy = array( 'industry', 'project-type' );
$term_list = wp_get_object_terms( $post->ID, $taxonomy );

// Set thumbnail background image
if ( get_the_post_thumbnail() ) {
  $thumb_background = get_the_post_thumbnail_url();
} else {
  $thumb_background = get_template_directory_uri() . '/assets/img/default.jpg';
} ?>

<div class="column iso-grid__item<?php

// Add class names based on tax terms
if ( ! empty( $term_list ) && ! is_wp_error( $term_list ) ) {
  foreach( $term_list as $term ) {
    echo ' ' . $term->slug;
  }
}
?>">

  <a href="<?php the_permalink(); ?>">
    <div class="project-list__item<?php if ( ! get_the_post_thumbnail() ) : ?> project-list__item--default<?php endif; ?>"
         style="background-image: url(<?php echo esc_url($thumb_background); ?>);">
    <?php if ( get_field('vimeo_id') ) : ?>
      <i class="project-list__item__play-icon icon icon--play"><span class="screen-reader-text"><?php _e( 'Play video', 'ssi' ); ?></span></i>
    <?php endif; ?>
      <div class="project-list__item__content">
        <div class="project-list__item__content__inner">
          <h3 class="project-list__item__title"><?php the_title(); ?></h3>
        </div>
      </div>
    </div>
  </a>

</div><!-- .column -->
