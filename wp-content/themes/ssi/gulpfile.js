'use strict';

// Include gulp
var gulp = require( 'gulp' );

// Include Our Plugins
var chmod         = require( 'gulp-chmod' );
var browserSync   = require( 'browser-sync' ).create();
var jshint        = require( 'gulp-jshint' );
var sass          = require( 'gulp-sass' );
var concat        = require( 'gulp-concat' );
var uglify        = require( 'gulp-uglify' );
var cleanCSS      = require( 'gulp-clean-css' );
var autoprefixer  = require( 'gulp-autoprefixer' );
var rename        = require( 'gulp-rename' );
var del           = require( 'del' );
var sourcemaps    = require( 'gulp-sourcemaps' );

// Working directories
var workingJS     = [ 'assets/js/*.js', 'assets/js/**/*.js' ];
var workingSCSS   = [ 'assets/sass/*.scss', 'assets/sass/**/*.scss' ];

// Distribution directories
var distJS        = 'dist/js';
var distCSS       = 'dist/css';

/*gulp.task( 'chmoddddd', function() {
  gulp.src( distJS )
    .pipe( chmod(0o755) )
    .pipe( gulp.dest( distJS ) );
  }
);*/

// Lint Task
gulp.task( 'lint', function() {
  return gulp.src( workingJS )
    .pipe( jshint() )
    .pipe( jshint.reporter( 'default' ) );
});

// Compile Our Sass
gulp.task( 'sass', function() {
  gulp.src( workingSCSS )
    .on( 'error', sass.logError )
    .pipe( sourcemaps.init() )
      .pipe( sass() )
      .pipe( autoprefixer( [ 'last 2 versions', 'ie >= 9', 'and_chr >= 2.3' ] ) )
      .pipe( gulp.dest( distCSS ) )
      .pipe( browserSync.stream() )
      .pipe( cleanCSS( {
        compatibility: 'ie8'
      }))
      .pipe( rename( {
        suffix: '.min'
      }))
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest( distCSS ) )
    .pipe( browserSync.stream() );
});

// Minify JS
gulp.task( 'scripts', function() {
  return gulp.src( workingJS )
    .pipe( gulp.dest( distJS ) )
    //.pipe( uglify() )
    .pipe( rename( {
      suffix: '.min'
    }))
    .pipe( gulp.dest( distJS ) );
});

// Watch Files For Changes
gulp.task( 'watch', function() {
  gulp.watch( workingJS, [ 'lint', 'scripts' ] );
  gulp.watch( workingSCSS, [ 'sass' ] );
});

// Default Task
gulp.task( 'default', [ 'lint', 'sass', 'scripts', 'watch' ] );
