<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header(); ?>

<main class="blog__posts-list section--padded" role="ajax_posts">
  <div class="row" data-equalizer data-equalize-on="medium">
    <div class="columns large-10 large-centered">
      <div class="row medium-up-2">

        <?php
        if ( have_posts() ) {
          $blog_post_count = 0;
          global $blog_post_count;
          while ( have_posts() ) {
            the_post();
            $blog_post_count++;
            get_template_part( 'template-parts/content', 'blog' );
          }
        } else {
          get_template_part( 'template-parts/content', 'none' );
        }
        ?>

      </div>
      <div class="row medium-up-2 blog__ajax-posts"></div>
      <button class="blog__more-posts" data-category="<?php echo esc_attr( $cat_id ); ?>"><?php esc_html_e( 'Load More', 'ssi' ); ?></button>

    </div>
  </div>
</main>

<?php // the_posts_navigation(); ?>

<?php get_footer();
