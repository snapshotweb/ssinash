<?php
/**
 * The sidebar containing the widget area for projects
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SnapShot_Interactive
 */ ?>

<aside id="secondary" class="widget-area single-project__sidebar" role="complementary">

  <div class="single-project__sidebar__widget single-project__sidebar__widget--share">
    <h4>Share</h4>
    <?php get_template_part( 'template-parts/nav', 'social-share' ); ?>
  </div>

  <?php
  $client = get_field('client');
  if ( $client ) :
    $post = $client;
    setup_postdata( $post ); ?>

    <div class="single-project__sidebar__widget single-project__sidebar__widget--client">

      <h4>Client</h4>
      <?php the_title(); ?>
    </div>

  <?php endif; wp_reset_postdata(); ?>

  <?php
  $team_members = get_field('team_members');
  if ( $team_members ) : ?>

    <div class="single-project__sidebar__widget single-project__sidebar__widget--team">

      <h4><?php _e( 'Team Members', 'ssi' ); ?></h4>
      <ul>
        <?php foreach ( $team_members as $post ) : setup_postdata($post); ?>
          <li>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>

  <?php endif; wp_reset_postdata(); ?>

  <?php
  $location = get_field('location');
  if ( $location ) : ?>

    <div class="single-project__sidebar__widget single-project__sidebar__widget--location">

      <h4>Location</h4>
      <?php
      // Set counter to determine last iteration of loop
      $i = 0;
      $len = count( $location ); ?>
      <p>
        <?php foreach ( $location as $post ) {
          setup_postdata($post);
          the_title();

          // Use commas unless last iteration
          if ( $i != $len - 1 ) {
            echo ', ';
          }
          $i++;
        } ?>
      </p>
    </div>

  <?php endif; wp_reset_postdata(); ?>

  <?php
  $taxonomy = 'industry';
  $industries = get_the_terms( get_the_ID(), $taxonomy );
  if ( $industries && ! is_wp_error( $industries ) ) : ?>

    <div class="single-project__sidebar__widget single-project__sidebar__widget--industries">
      <h4><?php _e( 'Industries', 'ssi' ); ?></h4>
      <?php echo strip_tags( get_the_term_list( get_the_ID(), $taxonomy, '', ', ', '' ) ); ?>
    </div>

  <?php endif; ?>

  <?php /* dynamic_sidebar( 'sidebar-1' ); */ ?>
</aside><!-- #secondary -->
