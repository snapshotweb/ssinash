<?php
/**
 * The template for displaying blog posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SnapShot_Interactive
 */

get_header();
$cat_id = get_query_var('cat');
?>

<?php get_template_part( 'template-parts/page/blog/nav', 'categories' ); ?>

<main class="blog__posts-list section--padded" role="ajax_posts">
  <div class="row" data-equalizer data-equalize-on="medium">
    <div class="columns large-10 large-centered">
      <div class="row medium-up-2">

        <?php
        if ( have_posts() ) {
          $blog_post_count = 0;
          global $blog_post_count;
          while ( have_posts() ) {
            the_post();
            $blog_post_count++;
            get_template_part( 'template-parts/content', 'blog' );
          }
        } else {
          get_template_part( 'template-parts/content', 'none' );
        }
        ?>

      </div>
      <div class="row medium-up-2 blog__ajax-posts"></div>
      <button class="blog__more-posts" data-category="<?php echo esc_attr( $cat_id ); ?>"><?php esc_html_e( 'Load More', 'ssi' ); ?></button>

    </div>
  </div>
</main>

<?php // the_posts_navigation(); ?>

<?php get_footer();
