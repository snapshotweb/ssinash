<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SnapShot_Interactive
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
  return;
}
?>

<?php if ( is_search() ) : ?>

  <h1 class="search-page-title"><?php echo sprintf( __( '%1$s results found for &lsquo;%2$s&rsquo;', 'ssi' ), esc_html( $wp_query->found_posts ), '<span class="search-term">' . esc_html( get_search_query() ) . '</span>' ); ?></h1>

  <?php get_search_form(); ?>

<?php else : ?>

  <aside id="secondary" class="widget-area" role="complementary">
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
  </aside><!-- #secondary -->

<?php endif; ?>
