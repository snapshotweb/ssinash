(function($) {

  // When the window has finished loading create our google map below
  google.maps.event.addDomListener( window, 'load', mapInit );

  function mapInit() {

    // Check for IE 11
    isIE11 = !! navigator.userAgent.match( /Trident.*rv\:11\./ );

    // Set mobile breakpoint
    var mobileBreak = 640;
        isMobile    = ( $(window).width() < mobileBreak );

    /**
     * Define map styles
     * @reference https://snazzymaps.com/style/38/shades-of-grey
     */
    var mapStyle=[{featureType:"all",elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#000000"},{lightness:40}]},{featureType:"all",elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#000000"},{lightness:16}]},{featureType:"all",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#000000"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#000000"},{lightness:17},{weight:1.2}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#000000"},{lightness:20}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#000000"},{lightness:21}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#000000"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#000000"},{lightness:29},{weight:0.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#000000"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#000000"},{lightness:16}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#000000"},{lightness:19}]},{featureType:"water",elementType:"geometry",stylers:[{color:"#000000"},{lightness:17}]}];

    // Define market icon properties
    var markerPNG = directoryURI.templateURL + '/assets/img/marker_map.png';
        markerSVG = directoryURI.templateURL + '/assets/img/marker_map.svg';
        markerIcon = {
          url: isIE11 ? markerPNG : markerSVG,
          size:       new google.maps.Size( 108, 140 ),
          origin:     new google.maps.Point( 0, 0 ),
          anchor:     new google.maps.Point( 27, 70 ),
          scaledSize: new google.maps.Size( 54, 70 )
        };

    /**
     * Options to create maps
     * @reference https://developers.google.com/maps/documentation/javascript/reference#MapOptions
     */
    var $nashButton = $('#map-button-nashville');
    var mapOptions = {
      zoom: 16,
      scrollwheel: false,
      draggable: false,
      zoomControl: false,
      scaleControl: false,
      disableDefaultUI: true,
      center: new google.maps.LatLng( $nashButton.data('lat'), $nashButton.data('lng') ),
      styles: mapStyle,
    };

    // Get the HTML DOM element that will contain the map
    var mapElement = document.getElementById('map-container');

    // Create the map
    var map = new google.maps.Map( mapElement, mapOptions );

    // Define location data based on button data attributes
    var $button = $('.contact-page__locations__tabs__link');
    $button.each( function () {
      var $lat = $(this).data('lat');
          $lng = $(this).data('lng');

      // Create map markers
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng( $(this).data('lat'), $(this).data('lng') ),
        map: map,
        title: 'SnapShot Interactive ' + $(this).data('location-name'),
        icon: markerIcon
      });

      // Recenter map on location button click
      google.maps.event.addDomListener(
        this,
        'click',
        function () {
          map.setCenter( new google.maps.LatLng( $(this).data('lat'), $(this).data('lng') ) );
        }
      );
    });

    // Toggle active state on buttons
    $button.click( function() {
      var $parent = $(this).parent();
          activeClass = 'is-active';
      $parent.addClass(activeClass);
      $parent.siblings().removeClass(activeClass);
    });
  }
})( jQuery );
