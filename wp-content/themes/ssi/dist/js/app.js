(function($) {

  // Initialize Foundation
  $(document).foundation();

  /**
   * Navigation menu
   */
  $(document).ready( function() {

    // If you change this breakpoint don't forget to update this value as well
    var MQL = 1024;

    // Primary navigation slide-in effect
    if ( $(window).width() > MQL ) {
      var headerHeight = $('.site__header').height();
      $(window).on('scroll', { previousTop: 0 }, function () {
        var currentTop = $(window).scrollTop();
        // Check if user is scrolling up
        if ( currentTop < this.previousTop ) {
          // If user is scrolling up...
          if ( currentTop > 0 && $('.site__header').hasClass('is-fixed') ) {
            $('.site__header').addClass('is-visible');
            $('.hamburger-icon span').addClass('active');
          } else {
            $('.site__header').removeClass('is-visible is-fixed');
          }
        } else {
          // If user is scrolling down...
          $('.site__header').removeClass('is-visible');
          if ( currentTop > headerHeight && ! $('.site__header').hasClass('is-fixed') ) {
            $('.site__header').addClass('is-fixed');
          }
        }
        this.previousTop = currentTop;
      });
    }

    // Open/close primary navigation
    $('.hamburger-icon').on( 'click', function() {
      $('.hamburger-icon').toggleClass('open');
      $('.site__header').toggleClass('menu-is-open');

      /**
       * In Firefox transitions break when parent overflow is changed,
       * so we need to wait for the end of the trasition to give the
       * body an overflow hidden.
       */
      if ( $('.site__nav__overlay').hasClass('is-visible') ) {
        $('.site__nav__overlay').removeClass('is-visible').one( 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
          $('body').removeClass('overflow-hidden');
        });
      } else {
        $('.site__nav__overlay').addClass('is-visible').one( 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
          $('body').addClass('overflow-hidden');
        });
      }
    });
  });

  /**
   * Search icon
   */
  $(document).ready( function() {
    $('.search-toggle__item--button').click( function() {
      $('.search-toggle__item--field').toggle('slide');
      $('.search-toggle').toggleClass('search-open');
    });
  });

  /**
   * Masonry
   *
   * Check first if Masonry is enqueued (set up as a variable in `enqueue-scripts.php`)
   */
  if ( scriptIsEnqueued.masonry ) {
    $('.grid').masonry({
      itemSelector: '.grid-item'
    });
  }

  /**
   * Isotope
   *
   * Check first if Isotope is enqueued (set up as a variable in `enqueue-scripts.php`)
   */
  if ( scriptIsEnqueued.isotope ) {

    // Project grid
    var $isogrid = $('.iso-grid').isotope({
      //// options
      itemSelector: '.iso-grid__item',
      layoutMode: 'fitRows'
    });
    //// bind filter on select change
    $('.filters-select').on( 'click', 'button', function() {
      //// get filter value from option value
      var filterValue = $( this ).attr('data-filter');
      $isogrid.isotope({ filter: filterValue });
    });
    //// change is-checked class on buttons
    $('.filters-select').each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
      });
    });

    // Team grid
    var $grid = $('.team-grid').isotope({
      itemSelector: '.team-grid__item',
      layoutMode: 'fitRows',
    });

    //// bind filter button click
    $('#team-buttons').on( 'click', 'button', function() {
      var filterValue = $( this ).attr('data-filter');
      $grid.isotope({ filter: filterValue });
    });

    //// change is-checked class on buttons
    $('.team-nav__button-group').each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
      });
    });

    $(document).load( function() {
      $grid.isotope('.team-grid').isotope();
    });
  } // End if ( scriptIsEnqueued.isotope )

  /**
   * Slick Slider
   *
   * Check first if Slick is enqueued (set up as a variable in `enqueue-scripts.php`)
   */
  if ( scriptIsEnqueued.slick ) {

    $('.single-project__testimonials__slider').slick({
      dots:     true,
      arrows:   false,
      infinite: true,
      speed:    500,
      fade:     true,
      cssEase:  'linear'
    });

    $('.web-browser-window__container').slick({
      dots: false,
      arrows: true,
      speed: 500
    });

    $('.team-bio-section--slider').slick({
      dots: true,
      arrows: false,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 3000,
      fade: true,
      cssEase: 'linear'
    });
  } // End if ( scriptIsEnqueued.slick )

  /**
   * Set value of select field on click for job application
   */
  $( ".careers__single-career__button--apply" ).click(function() {
    $("#open-positions").val( $(this).data('value') );
  });

  /**
   * Add span wrapper around first name on team member single pages
   */
  (function() {
    var node = $('.page-header--team__name').contents().filter( function () { return this.nodeType == 3; }).first(),
        text = node.text(),
        first = text.slice(0, text.indexOf(" "));

    if ( ! node.length ) {
      return;
    }

    node[0].nodeValue = text.slice( first.length );
    node.before('<span class="page-header--team__name__first">' + first + '</span>');
  })();

  /**
   * Ajax post loader
   */
  var $content = $('.blog__ajax-posts');
  var $loader = $('.blog__more-posts');
  var cat = $loader.data('category');
  var ppp = 6;
  var offset = $('.blog__posts-list').find('.blog__post').length;

  $loader.on( 'click', load_ajax_posts );

  function load_ajax_posts() {

    if ( ! ( $loader.hasClass('blog__more-posts--posts-loading') || $loader.hasClass('blog__more-posts--no-more-posts') ) ) {
      $.ajax({
        type: 'POST',
        dataType: 'html',
        url: ajaxLoadPosts.ajaxurl,
        data: {
          'cat': cat,
          'ppp': ppp,
          'offset': offset,
          'action': 'ssi_more_post_ajax'
        },
        beforeSend : function () {
          $loader.addClass('blog__more-posts--posts-loading').html(ajaxLoadPosts.loading);
        },
        success: function (data) {
          var $data = $(data);
          if ( $data.length ) {
            var $newElements = $data.css({ opacity: 0 });
            $content.append($newElements);
            $newElements.animate({ opacity: 1 });
            $loader.removeClass('blog__more-posts--posts-loading').html(ajaxLoadPosts.loadmore);
          }
          else {
            $loader.removeClass('blog__more-posts--posts-loading').addClass('blog__more-posts--no-more-posts').html(ajaxLoadPosts.noposts);
          }
        },
        error : function ( jqXHR, textStatus, errorThrown ) {
          $loader.html( $.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown );
          console.log( jqXHR );
        },
      });
    }
    offset += ppp;
    return false;
  } // End load_ajax_posts

  /**
   * Add class to select elements using SVGs when loading on Safari to
   * use PNG fallbacks and prevent hover bug.
   *
   * @link https://stackoverflow.com/questions/29644539/svg-background-image-strange-transition-behavior-on-hover
   */
  if ( navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1 )  {
    $('*').each( function() {
      if ( ! $(this).css('background-image') ) {
        return;
      } else if ( ~ $(this).css('background-image').indexOf(".svg") ) {
        $(this).addClass('safari');
      }
    });
  }

})( jQuery );
