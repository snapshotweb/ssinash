<?php if(! $_POST ): ?>

<form <form action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">

<table>
  <tr>
    <td>
      Name:
    </td>
    <td>
      <input name="name" type="text" />
    </td>
  </tr>
  <tr>
    <td>
      Email Name:
    </td>
    <td>
      <input name="email_name" type="text" placeholder="chance" />
    </td>
  </tr>
  <tr>
    <td>
      Email Domain:
    </td>
    <td>
      <input name="email_domain" type="text" value="snapshotinteractive" /> no dot com. (Thank Chance)
    </td>
  </tr>
  <tr>
    <td>
      Title:
    </td>
    <td>
      <input name="title" type="text" />
    </td>
  </tr>
  <tr>
    <td>
      Phone:
    </td>
    <td>
      <input name="phone" value="615.810.9855" type="text" />
    </td>
  </tr>
  <tr>
    <td>
      Website:
    </td>
    <td>
      <input name="url" value="snapshotinteractive.com" type="text" />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <input type="submit" value="View Signature" />
    </td>
  </tr>
</table>

</form>

<?php else: ?>

  <div style="color:#2c2c2c;font-family:'Century Gothic', 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 12px; margin-bottom: 24px; Margin-bottom: 24px;">
  	<span style="font-size:20px;font-weight:bold;"><?php echo $_POST['name']; ?></span><br />
  	<span style="text-transform:uppercase;"><?php echo $_POST['title']; ?></span><br />
  	<a href="mailto:<?php echo $_POST['email_name']; ?>@<?php echo $_POST['email_domain']; ?>.com" target="_blank" style="color:#2c2c2c;text-decoration:none;"><?php echo $_POST['email_name']; ?><span style="color:#EDAC2C;">@</span><?php echo $_POST['email_domain']; ?><span style="color:#EDAC2C;">.</span>com</a><br />
  	<span style="color:#EDAC2C;">P:</span> <?php echo $_POST['phone']; ?><br />
  </div>
  <div style="color:#2c2c2c;font-family:'Century Gothic', 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 12px;">
  	<a style="text-decoration: none; color: #2c2c2c" href="http://<?php echo $_POST['url']; ?>" target="_blank"><img src="http://snapshotinteractive.com/signature/images/ssi_logo.png" alt="SnapShot Interactive" style="border-style:none;margin-bottom:6px; Margin-bottom: 6px;" /></a><br />
  	<a  href="http://snapshotinteractive.com/our-work/#web" target="_blank" style="color:#2c2c2c;text-decoration:none;font-weight:bold;">WEB</a> <span style="color:#EDAC2C;">|</span> <a href="http://snapshotinteractive.com/our-work/#video" target="_blank" style="color:#2c2c2c;text-decoration:none;font-weight:bold;">VIDEO</a> <span style="color:#EDAC2C;">|</span> <a href="http://snapshotinteractive.com/our-work/#animation" target="_blank" style="color:#2c2c2c;text-decoration:none;font-weight:bold;">ANIMATION</a>
  </div>

<?php endif; ?>
